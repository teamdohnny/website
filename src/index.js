import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import theme from './Files/Settings/Styles/theme';
import {ThemeProvider} from 'styled-components';
import { BrowserRouter } from 'react-router-dom';
import ModalProvider from "Components/Providers/Modals";
import ClayProvider from 'local_npm/clay-components';
import "static/fonts/Gordita.css";

//import * as serviceWorker from './serviceWorker';
//import { loadReCaptcha } from 'react-recaptcha-v3';
//))-------------------

const Init = ()=>{

	/*
		useEffect(()=>{
			loadReCaptcha("6LeLSbYUAAAAAGgSDOXOF5de5WWr0Fz-JjkyF0ut")
		});

	*/

	return (<BrowserRouter>
	<ThemeProvider theme={theme}>
		
	<ClayProvider>
	<ModalProvider>
	
			<App />
		
				</ModalProvider>
		</ClayProvider>

	</ThemeProvider></BrowserRouter> 
);}

ReactDOM.render(<Init />, document.getElementById('root'));


// Error:
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();