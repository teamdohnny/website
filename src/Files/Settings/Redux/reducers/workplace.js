export default function reducer( state = { 
  fetchRequest:null, //None, feching, done
  url: null,
  project: null, 
  component: null,
  header: null,
  sidebar: false,
  editor: null,
  page: "main_page",
  redirect: null,
  loading: true,
}, action) 
{   
  switch (action.type)
  {
      case "FETCH":  
      {
        return {...state, fetchRequest: "FECHING" }
      }
      case "FETCHED":  
      {
        return {...state, fetchRequest: "DONE" }
      }
      case "SELECT_PROJECT":  
      {
      	return {...state, project: action.payload }
      }
      case "SELECT_EDITOR":  
      {
        return {...state, editor: action.payload }
      }
      case "ADD_COMPONENTOBJ":  
      {
        return {...state, component: action.payload }
      }
      case "ADD_URL":  
      {
        return {...state, url: action.payload }
      }

      case "TOGGLE_NOTEBOOK":
      {
        return {...state, notebook:!state.notebook}
      }

      case "TOGGLE_SIDEBAR":
      {
        return {...state, sidebar:!state.sidebar}
      }
      
      case "EDIT_HEADER":
      {
        return {...state, header:action.payload}
      }
         case "SELECT_PAGE":  
      {
        return {...state, page: action.payload }
      }
        case "LOADED":  
      {
        return {...state, loading: false}
      }  
      case "REDIRECT":
      {
        return {...state, redirect: action.payload, go: null }
      }
      case "SELECT_JUST_PROJECT":
      {
        return {...state, project: null, 
                          component: [],
                          editor: null
                }
      }

    
     
  } 
    return state
}

