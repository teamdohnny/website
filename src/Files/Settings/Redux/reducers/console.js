const uuidv4 = require('uuid/v4');
export default function reducer(state = {
  action:null,
  isActive: true,
  contexts:[],
  log: [],
  response: null,
  result: null,
  user_says: null,
  speech:null,
},action){   


/*
{
  "action": null,
  "contexts": [],
  "log": [],
  "response": {
    "id": "ea19398d-6b75-4b26-bbf1-5765caede8d1",
    "timestamp": "2017-11-17T19:14:38.662Z",
    "lang": "es",
    "result": {
      "source": "agent",
      "resolvedQuery": "crea componente",
      "action": "crea.molecule",
      "actionIncomplete": true,
      "parameters": {
        "adjetivo": [],
        "adjetivo-contextual": [],
        "color": [],
        "componente": ""
      },
      "contexts": [ ],
      "metadata": {
        "intentId": "6319c54f-8aed-41ae-905a-b684799dda83",
        "webhookUsed": "false",
        "webhookForSlotFillingUsed": "false",
        "intentName": "Create-Molecule"
      },
      "fulfillment": {
        "speech": "What is the componente?",
        "messages": [
          {
            "type": 0,
            "speech": "What is the componente?"
          }
        ]
      },
      "score": 0.46000000834465027
    },
    "status": {
      "code": 200,
      "errorType": "success",
      "webhookTimedOut": false
    },
    "sessionId": "developer01"
  },


  "user_says": "crea componente",
  "speech": "crea componente",
  "result": {

    "source": "agent",

    "resolvedQuery": "crea componente",

    "action": "crea.molecule",

    "actionIncomplete": true,

    "parameters": {
      "adjetivo": [],
      "adjetivo-contextual": [],
      "color": [],
      "componente": ""
    },

    "contexts": [],

    "metadata": {},

    "fulfillment": {
      "speech": "What is the componente?",
      "messages": [
        {
          "type": 0,
          "speech": "What is the componente?"
        }
      ]
    },
    "score": 0.46000000834465027
  }
}






  */
    switch (action.type) {
      case "ADD_RESPONSE": 
      {


      	return {...state, response: action.payload,
                          user_says: action.payload.result.resolvedQuery,
                          result: action.payload.result,
                          speech: action.payload.result.fulfillment.speech,    
                }
      }

          case "IS_ACTIVE": 
      {


        return {...state, isActive: action.payload}
      }

       case "CLEAN_RESPONSE": 
      {
        return {...state, response: null,
                          user_says: null,
                          result: null,
                          speech: null 
                }
      }



      case "NEW_INPUT": 
      {
         return {...state, user_says: action.payload}
      }
       case "ADD_SPEECH": 
      {
         return {...state, speech: action.payload}
      }
      case "PUSH_LOG": 
      {
         return {...state, log: [...state.log, action.payload]}
      }
    
    }
    return state
}

