import { combineReducers } from "redux"
import profile from "./profile";
import workplace from "./workplace";
import chatbot from "./console";
import xray from "./xray";

export default combineReducers({
  profile, //Required*
  workplace,//Dont required now
  chatbot, //Required :) Open chanel to all the app. ***
  xray,//Required :) Open chanel to all the app (Provider?)
})
