export default function reducer(state = {
  authenticated: false,
  user: {photoURL: null, displayName:null}
  }, action) {

    switch (action.type) {
      case "SET_USER": {
        return {...state, user: action.payload,authenticated: true }
      }

      case "GET_USER_ACCESS": {
        return {...state, authenticated: true}
      }
      case "REMOVE_USER_ACCESS": {
        return {...state, authenticated: false, user: {photoURL: null, displayName:null}}
      }
    }

    return state
}

