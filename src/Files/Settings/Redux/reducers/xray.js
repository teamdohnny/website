export default function reducer( state = { 
component: null,
url:null,
updateComponent: {
  style:()=>{},
  code:()=>{},
}
}, action) 
{   
  switch (action.type)
  {
      case "ADD_COMPONENT_TO_XRAY":  
      {
        return {...state, component: action.payload }
      }
      case "UPDATE_URL_PATH":  
      {
        return {...state, url: action.payload }
      }
      case "ADD_CURRENT_FRAME":  
      {
        return {...state, updateComponent: action.payload }
      }
      case "SELECT_PROJECT":  
      {
      	return {...state, project: action.payload }
      }
      case "SELECT_EDITOR":  
      {
        return {...state, editor: action.payload }
      }
         
  } 
    return state
}

