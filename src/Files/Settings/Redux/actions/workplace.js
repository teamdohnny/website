export function selectProject(id = null) {

  return {
    type: "SELECT_PROJECT",
    payload: id
  }
}


export function selectPage(id = null) {

  return {
    type: "SELECT_PAGE",
    payload: id
  }
}

export function addComponent(component = null) {

  return {
    type: "ADD_COMPONENTOBJ",
    payload: component
  }
}



export function fetch() {

  return {
    type: "FETCH"
  }
}


export function loaded() {

  return {
    type: "LOADED"
  }
}

export function selectEditor(id = null) {

  return {
    type: "SELECT_EDITOR",
    payload: id
  }
}

export function pushComponent( component) {

  return {
    type: "PUSH_COMPONENT_2",
    payload:  component
  }
}

export function popComponent() 
{

  return {
    type: "POP_COMPONENT"
  }
}


export function cleanComponent() 
{

  return {
    type: "CLEAN_COMPONENT"
  }
}

export function toggleNotebook() 
{
  return {
    type: "TOGGLE_NOTEBOOK"
  }

}

export function toggleSidebar() 
{
  return {
    type: "TOGGLE_SIDEBAR"
  }

}


export function setCurrentUrl(url) 
{
  return {
    type: "ADD_URL",
    payload: url
  }
}


export function changeHeader(name) 
{
  return {
    type: "EDIT_HEADER",
    payload: name
  }
}

export function redirect(url = null) {
  return {
    type: "REDIRECT",
    payload: url
  }
}

export function projectSetup(url = null) {
  return {
    type: "REDIRECT",
    payload: url
  }
}