const uuidv4 = require('uuid/v4');

export function update(data) 
{

  return {
    type: 'ADD_COMPONENT_TO_XRAY',
    payload: data
  }
}




export function updateuRL(data) 
{

  return {
    type: 'UPDATE_URL_PATH',
    payload: data
  }
}


export function setCurrentFrame(data) 
{

  return {
    type: 'ADD_CURRENT_FRAME',
    payload: data
  }
}

export function newInput(data) 
{

  return {
    type: 'NEW_INPUT',
    payload: data,
 
  }
}


export function cleanResponse(data) 
{
  return {
    type: 'CLEAN_RESPONSE',
    payload: data,
 
  }
}

