const uuidv4 = require('uuid/v4');

export function addResponse(data) 
{
  return {
    type: 'ADD_RESPONSE',
    payload: data
  }
}

export function addSpeech(data = null) 
{
  return {
    type: 'ADD_SPEECH',
    payload: data
  }
}

export function newInput(data) 
{
  return {
    type: 'NEW_INPUT',
    payload: data,
 
  }
}

export function cleanResponse(data) 
{
  return {
    type: 'CLEAN_RESPONSE',
    payload: data,
 
  }
}

