import { /*applyMiddleware, */createStore } from "redux";
import reducer from "./reducers";
//import {loadState, saveState} from "../tools/db/localStorage"

//const _ = require("lodash");

const Root = () => {
	const initialState = undefined; //loadState()
	const store = createStore(reducer, initialState);
	return store;
};

export default Root;
