import gql from "graphql-tag";

export const MutationDeleteProject = gql(`
mutation($id: ID!) {
  deleteProject(input: {id:$id}) {
    id
  }
}`);



export const MutationUpdateProject = gql(`
mutation($id: ID! $name:String $description:String $updatedAt: String $config: String)
{
  updateProject(input: {id:$id name: $name  description:$description  updatedAt: $updatedAt config: $config})
  {
    id
    name
    description
    privacy
    style
    updatedAt
    config
  }
}`);

 
export const MutationCreateProject = gql(`
  mutation(
    $ownerId: String!
    $name: String!
    $privacy: String!
    $createdAt: String!
    $description: String
    $style: String
    $forkId: String
    ){

      createProject(input:{
        ownerId: $ownerId
        description:$description
        name:$name
        privacy:$privacy
        updatedAt:$createdAt
        createdAt:$createdAt
        style: $style
        forkId: $forkId
      }){
        id
        name
        description
        privacy
        style
        updatedAt
      }
  }`);


