import { graphql, compose } from "react-apollo";
import gql from "graphql-tag";

import {FRAGMENT_FULLCOMPONENT, FRAGMENT_SIMPLE_COMPONENT} from "../../Components/Queries/fragments";


export const QueryAllProjectsUser = gql(`
 
query ($id: String!){
  listProjectsUser(
    ownerId: $id
  ){
    items{
      id
      forkId
      name
      style
      description
      config  
    }
  } 
}
`);

export const QueryAllProjects = gql(`
  query {
    listProjects {
      items {
        id
        forkId
        name
        style
      }
    }
  }`);

export const MutationDeleteProject = gql(`
  mutation($id: ID!) {
    deleteProject(input: {id:$id}) {
      id
    }
  }`);

export const QueryGetComponent = gql(`
query($id: ID!) {
  getComponent(id: $id) {
  id
  name
  atoms(limit: 50){
      items
      {
        id
        place
        type
        inline
        render
        style
        locked
        value
        createdAt
        updatedAt
      }
    }
  }
}`);


export const QueryGetProject = gql(`
query($id: ID!) {
  getProject(id: $id) {
  createdAt
  description
  id
  forkId
  name
  ownerId
  privacy
  style
  updatedAt
  config

  }
}`);



export const QueryGetProjectWithComponentNameList = gql(`
query($id: ID!) {
  getProject(id: $id) {
  createdAt
  description
  id
  forkId
  name
  ownerId
  privacy
  style
  config
  updatedAt
  components(limit: 50){
    items{
      id
      name
    }
  }

  }
}`);
  
export const QueryGetFullProject = gql(`
${FRAGMENT_FULLCOMPONENT}
query($id: ID!) {
  getProject(id: $id) {
  createdAt
  description
  id
  forkId
  name
  ownerId
  privacy
  style
  updatedAt
  config

  components(limit:50){
    items{
     ...FullComponent
    }
  }




  }
}`);

export const QueryGetNormalProject = gql(`
${FRAGMENT_SIMPLE_COMPONENT}
query($id: ID!) {
  getProject(id: $id) {
  id
  forkId
  description
  name
  ownerId
  privacy
  style
  config


  }
}`);
