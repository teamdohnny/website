import { graphql } from "react-apollo";

import { QueryAllProjects, QueryGetProject,QueryAllProjectsUser, QueryGetFullProject } from "../Queries/Fetch"


/*
  fetchAllProjects:
  this fetch the list of all the public Projects usefull in Comunity:
*/
export const fetchAllProjects = graphql(
          QueryAllProjectsUser,
          {
              options: (props)=> ({
                                fetchPolicy: 'cache-and-network',
                                variables: {id: props.user.user.uid
                                }
                            }),
              props: (props) => {

                const  { data: { listProjectsUser = { items: [] }, loading, error }, data  } = props
               
                return {
                  fetchAllProjects: data, // All the data of the query
                  projects: listProjectsUser.items,
                  loadingProject:loading,
                  error
                }

              }
          }
      )

/*
  graphQueryReadProject:
  this fetch the list of all the public Projects usefull in Comunity:
*/
export const graphQueryReadProject = graphql(
        QueryGetProject,
        {
            options: ({ match: { params: { id } } }) => ({

                variables: {id},
                fetchPolicy: 'cache-and-network',
            }),
            props: ({ data, data: { getProject: project = null }  }) => ({
                project,
                fetchingProject: data.loading,
            }),
        },
    )

/*
  graphQueryReadFullProject:
  this fetch the list of all the public Projects usefull in Comunity:
*/
export const graphQueryReadFullProject = graphql(
        QueryGetFullProject,
        {
            options: ({ match: { params: { id } } }) => ({
                variables: { id },
                fetchPolicy: 'cache-and-network',
            }),
            props: ({ data: { getProject: fullProject }, data }) => {
              console.log(data)
              return ({
                            fullProject,
                            loading:data.loading,
                        })},
        },
    )




export const graphQueryReadFullProjectComponent = graphql(
        QueryGetFullProject,
        {
            options: ({  match: { params: { projectId } }  }) => ({
                variables: { id:projectId },
                //fetchPolicy: 'cache-and-network',
            }),
            props: ({ data: { getProject: fullProject }, data }) => {
             // console.log("El buenos dias a la vida", data)
              return ({
                            fullProject,
                            loadingProject:data.loading,
                        })},
        },
    )

