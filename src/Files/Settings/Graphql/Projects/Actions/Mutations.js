import gql from "graphql-tag";
import { graphql } from "react-apollo";
import { MutationDeleteProject,MutationUpdateProject,
MutationCreateProject } from "../Queries/Mutations"
import { QueryAllProjects, QueryGetProject } from "../Queries/Fetch"

//Simple mutation updaes are ASYNC so don't need to show fake data for now
export const graphQueryUpdateProject = graphql(
        MutationUpdateProject,
        {
        props: (props) => ({
          updateProject: (project) => {
              return props.mutate({
                  variables: {...project },
              });
          }
        })
        }
    );


//Simple mutation delete are ASYNC so don't need to show fake data for now
export const graphQueryDeleteProject = graphql(
        MutationDeleteProject,
        {
            options: props=>({
                updates: (proxy, { data: { deleteComponent } }) => {
                  
                   const query = QueryGetProject;
                   const variables = { id: props.match.params.id };

                   const data = proxy.readQuery({ query, variables });
                   data.getProject.components.items = data
                    .getProject
                      .components
                        .items
                          .filter(c => c.id !== deleteComponent.id);
          
                   proxy.writeQuery({ query, data });
                }
            }),
            props: (props) => ({
                deleteProject: (project) => {
                    return props.mutate({
                        variables: {...project }
                    });
                }
            })
        })
/*
==================================================================================
==================================================================================
==================================================================================

From here everything is a mess

==================================================================================
==================================================================================
==================================================================================
*/

//Projects are created using the special punctiion withApollo :)
export const graphQueryCreateProject = graphql(
        MutationCreateProject,
        {
        props: (props) => ({
          createProject: (project) => {
            console.log(project)
              return props.mutate({
                  variables: {...project },
                  /*
                  optimisticResponse: () => ({
                      updateProject: {
                         ...project,  __typename: 'Project'
                      }
                  }),*/
              });
          }
        })
        }
    );

export const deleteProjectMutation = graphql(
        MutationDeleteProject,
        {
            options: {
               //refetchQueries: [{ query: QueryAllProjects }], Temporal Out of service.
                update: (proxy, { data: { deleteProject } }) => {
                    const query = QueryAllProjects;
                    const data = proxy.readQuery({ query });

                    data.listProjects.items = data.listProjects.items.filter(project => project.id !== deleteProject.id);

                    proxy.writeQuery({ query, data });
                }
            },
            props: (props) => ({
                deleteProject: (project) => {
                    return props.mutate({
                        variables: { id: project.id },

                        optimisticResponse: () => ({
                            deleteProject: {
                               ...project,  __typename: 'Project'
                            }
                        }),
                    });
                }
            })
        }
    )