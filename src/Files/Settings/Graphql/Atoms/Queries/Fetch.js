import gql from "graphql-tag";

export const QueryGetAtom = gql(`
query($id: ID!) {
  getAtom(id: $id) {
          id
          place
          type
          inline
          render
          style
          locked
          value
          createdAt
          updatedAt 
  }
}`);