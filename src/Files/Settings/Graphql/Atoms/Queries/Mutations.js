import gql from "graphql-tag";

export const QueryGetComponent = gql(`
query($id: ID!) {
  getComponent(id: $id) {
  id
  name
  atoms{
      items
      {
    id
    place
    type
    inline
    render
    small
    style
    value
    locked
    createdAt
    updatedAt
      }
  }
  }
}`);

export const MutationCreateAtom = gql(`
mutation MutationCreateAtom(
  $referenceId: String!
  $place: Int!
  $type: String!
  $inline: Boolean
  $render: Boolean
  $small: Boolean
  $style: String
  $value: String
  $locked: String
  $createdAt: String!
 ){

  createAtom(input:{
  referenceId: $referenceId
  place: $place
  type: $type
  inline: $inline
  render: $render
  small: $small
  style: $style
  value: $value
  locked: $locked
  createdAt: $createdAt
  updatedAt: $createdAt
  }) {
  	id
  	place
  	type
  	inline
  	render
    small
  	style
  	value
    referenceId
  	locked
  	createdAt
  	updatedAt
  }
}`);


export const MutationDeleteAtom = gql(`
  mutation MutationDeleteAtom($id: ID!){
    deleteAtom(input: {id: $id}){
        id
    }
  }
`);

export const MutationUpdateAtom = gql(`
mutation($id: ID!  $value:String) {
  updateAtom(input: {id:$id   value:$value }) {
    id
    value
    updatedAt
  }
}
`);

export const MutationUpdateOptionsAtom = gql(`
mutation(
$id: ID!  
$inline: Boolean
$render: Boolean
$small: Boolean


$style: String
$locked: String
$updatedAt:String


){
  updateAtom(input: {
                id:$id  
                inline: $inline
                render: $render
                small:$small

                style: $style
                locked: $locked
                updatedAt: $updatedAt
            
  
                
              }){
  id
  place
  type
  inline
  render
  style
  locked
  small

  }
}
`);



export const MutationUpdatePositionAtom = gql(`
mutation(
$id: ID!  
$place: Int!
$updatedAt:String

){
  updateAtom(input: {
                id:$id
                updatedAt: $updatedAt
                place: $place
              }){
  id
  place
  type
  inline
  render
  style
  locked
  small

  }
}
`);







/*
  id: ID!
  referenceId: String!
  place: Int!
  type: String!
  inline: Boolean
  render: Boolean
  small: Boolean
  style: String
  value: String
  locked: String
  createdAt: String!
  updatedAt: String!

*/