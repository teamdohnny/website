import { graphql } from "react-apollo";

import {QueryGetAtom} from "../Queries/Fetch"

export const graphQueryReadAtom = graphql(
        QueryGetAtom,
        {
            options: ({ match: { params: { id } } }) => {
            
                return{
                variables: { id },

         
            }},
            props: ({ data: { getAtom: atom }, loading }) => ({
                atom,
                loading,
            }),
        },
    )



