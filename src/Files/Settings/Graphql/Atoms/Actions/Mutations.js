import { graphql } from "react-apollo";
import {
    MutationCreateAtom, 
    MutationUpdateAtom, 
    MutationDeleteAtom, 
    MutationUpdateOptionsAtom, 
    MutationUpdatePositionAtom
    } from "../Queries/Mutations"

import {QueryGetComponent} from "Files/Settings/Graphql/Components/Queries/fetch"
 
const uuidv4 = require('uuid/v4');

//Check this, because is Optimistic is not requeried in theory.
const CreateConfig = {

        options: props => ({

            update: (proxy, { data: { createAtom } }) => {
                
                const query = QueryGetComponent;// Get component
                const variables = { id: createAtom.referenceId };//component id

                const data = proxy.readQuery({ query, variables });
                     
                data.getComponent = { 

                    ...data.getComponent,
                    atoms: {

                        ...data.getComponent.atoms,
                        items: [
                            ...data.getComponent.atoms.items.filter(c => c.id !== createAtom.id),
                            createAtom,
                        ]
                    }
                };
                proxy.writeQuery({ query, data });
            },
        }),
        
        props: props => ({
            createAtom: (atom) => {
                return props.mutate({
                    variables: { ...atom },
                    
                   optimisticResponse: { 
                        createAtom: { 
                        ...atom, 
                        id:uuidv4(),
                        __typename: 'Atom'
                    } 
                    },
                });
            }
        })
    }
 
const UpdateConfig =  {

           options: props => ({

            updated: (proxy, { data: { updateAtom } }) => {
                const query = QueryGetComponent;//Get component
                const variables = { id: props.component };//component id
                const data = proxy.readQuery({ query, variables });
                const index = data.getComponent.atoms.items.filter((atom, index)=> {if(atom.id === updateAtom.id)return index})[0]
                 data.getComponent.atoms.items[index] = updateAtom
                proxy.writeQuery({ query, data });

            },
        }),
            props: (props) => ({
                updateAtom: (atom) => {

                    return props.mutate({
                        variables: {...atom },
                       
                    });
                }
            })
        }


const UpdateOptionsConfig =  {


           options: props => ({
            update: (proxy, { data: { updateAtom } }) => {
                const query = QueryGetComponent;//Get component
                const variables = { id: props.component };//component id
                const data = proxy.readQuery({ query, variables });
                const index = data.getComponent.atoms.items.filter((atom, index)=> {if(atom.id === updateAtom.id) return index}  )[0]
                 data.getComponent.atoms.items[index] = updateAtom
                proxy.writeQuery({ query, data });
            },
        }),
            props: (props) => ({
                changeAtom: (atom) => {

                    return props.mutate({
                        variables: {...atom },
                    });
                }
            })
        }

const DeleteConfig = {

          options: props => ({

            update: (proxy, { data: { deleteAtom } }) => {
                    //alert("Not working...")
                    const query = QueryGetComponent;
                    const data = proxy.readQuery({ query });
                    data.getComponent.atoms.items = data.getComponent.atoms.items.filter(atom => atom.id !== deleteAtom.id);
                    proxy.writeQuery({ query, data });
                }
              }),

            props: (props) => ({

                deleteAtom: (index) => {

                    return props.mutate({

                        variables: { id: index },
                
                    });
                }
            })
        }

const CreateConfigNoUI = {    
        props: props => ({
            createAtom: (atom) => {
                return props.mutate({
                    variables: { ...atom }
                });
            }
        })
    }






const UpdateOptionsConfig2 =  {


           options: props => ({
            update: (proxy, { data: { updateAtom } }) => {
                const query = QueryGetComponent;//Get component
                const variables = { id: props.component };//component id
                const data = proxy.readQuery({ query, variables });
                const index = data.getComponent.atoms.items.filter((atom, index)=> {if(atom.id === updateAtom.id) return index}  )[0]
                 data.getComponent.atoms.items[index] = updateAtom
                proxy.writeQuery({ query, data });
            },
        }),
            props: (props) => ({
                changePosition: (atom) => {

                    return props.mutate({
                        variables: {...atom },
                    });
                }
            })
        }


export const graphQueryCreateAtomNoUI = graphql(MutationCreateAtom, CreateConfigNoUI)
export const graphQueryCreateAtom = graphql(MutationCreateAtom, CreateConfig)
export const graphQueryUpdateAtom = graphql(MutationUpdateAtom, UpdateConfig)
export const graphQueryDeleteAtom = graphql(MutationDeleteAtom, DeleteConfig)
export const graphQueryUpdateOptionsAtom = graphql(MutationUpdateOptionsAtom, UpdateOptionsConfig)
export const graphQueryUpdatePositonsAtom = graphql(MutationUpdatePositionAtom, UpdateOptionsConfig2)