import  {ATOMS_LIMIT, COMPONENT_LIMIT} from "Files/Settings/Constants" 

//UPDATE FRAGMENT NAME fragment BasicDataOnComponent

export const FRAGMENT_FULLCOMPONENT = `


  fragment BasicDataOnComponent on Component {
        id
        name
        description
        privacy
        type
        connect
        imports
        parent{
        id
        name
        parent{
          id
          name
          parent{
            id
            name
            parent{
              id
              name
              parent{
                id
                name
              }
            }
          }
        }
      }
        project{
          id
          name
          ownerId
          privacy
        }    
  atoms(limit:${ATOMS_LIMIT}){
    items{
      id
      place
      type
      inline
      render
      small
      style
      value
      locked
      createdAt
      updatedAt
    } 
  }
}

  fragment FullComponent on Component {
         ...BasicDataOnComponent
          components(limit:${COMPONENT_LIMIT})
          {
            items
            {
              ...BasicDataOnComponent
              components(limit:${COMPONENT_LIMIT})
              {
                items
                {
                ...BasicDataOnComponent
                  components(limit:${COMPONENT_LIMIT})
                  {
                    items
                    {
                    ...BasicDataOnComponent
                      components(limit:${COMPONENT_LIMIT})
                      {
                        items
                        {
                          ...BasicDataOnComponent
                        }
                      }
                    }
                  } 
                }
              } 
            }
          }
  }`
;

export const FRAGMENT_SIMPLE_COMPONENT = `

  fragment BasicDataOnComponent1 on Component {
    id
    name
    description
    type
    connect

    imports
    atoms(limit:${ATOMS_LIMIT})
    {
      items
      {
            id
            place
            type
            inline
            render
            small
            style
            value
            locked
            createdAt
            updatedAt
      } 
    }
  }


  fragment FullComponent on Component {
         ...BasicDataOnComponent1
          components(limit:${COMPONENT_LIMIT})
          {
            items
            {
              ...BasicDataOnComponent1
              components(limit:${COMPONENT_LIMIT})
              {
                items
                {
                ...BasicDataOnComponent1
                  components(limit:${COMPONENT_LIMIT})
                  {
                    items
                    {
                    ...BasicDataOnComponent1
                      components(limit:${COMPONENT_LIMIT})
                      {
                        items
                        {
                          ...BasicDataOnComponent1
                        }
                      }
                    }
                  } 
                }
              } 
            }
          }
  }
`;