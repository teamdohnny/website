import gql from "graphql-tag";
import {FRAGMENT_SIMPLE_COMPONENT, FRAGMENT_FULLCOMPONENT} from "./fragments"

/*
  Fetch a component with all the elements isede and its projects
*/

export const getComponentOnComponentFilter = gql(`
query($referenceId: String! $type: String!) {
  getComponentoComponentFilter(referenceId: $referenceId, type: $type, limit: 50) {
  items{  
    description
    id
    name
  }
 

  }
}`);


export const getComponentOnComponent = gql(`
query($referenceId: String! ) {
  getComponentOnComponent(referenceId: $referenceId) {
 
  items{
    
  description
  id
  name


  }
 

  }
}`);





export const QueryGetComponent = gql(`
query($id: ID!) {
  
  getComponent(id: $id) {
    id
    name
    connect
    imports
    description
    privacy
    type

    project{
      id
      name
      ownerId
      privacy
    }

    parent{
      id
      name
      parent{
        id
        name
        parent{
          id
          name
          parent{
            id
            name
            parent{
              id
              name
            }
          }
        }
      }
    } 
    
  atoms(limit: 50)
  {
  items
  {
    id
    place
    type
    inline
    render
    small
    style
    value
    locked
    createdAt
    updatedAt
  }
  }
  }
}`);

/*
  Fetch a component with all the elements isede and its projects
*/
export const QueryGetProject = gql(`
query($id: ID!) {
  getProject(id: $id) {
    createdAt
    description
    id
    name
    ownerId
    privacy
    style
    updatedAt
  }
}
`);



/*
  Fetch a component with all the elements isede and its projects
*/

export const QueryGetNormalComponent = gql(`
${FRAGMENT_SIMPLE_COMPONENT}
query($id: ID!) {
  getComponent(id: $id) {
    ...FullComponent  
  }
}`);


/*
  Fetch a component with all the elements isede and its projects
*/
export const QueryGetFullComponent = gql(`
${FRAGMENT_FULLCOMPONENT}
query($id: ID!) {
  getComponent(id: $id) {
    ...FullComponent
    }
}`);
