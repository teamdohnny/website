import gql from "graphql-tag";

export const MutationUpdateComponent = gql(`
mutation($id:ID! $name:String $description:String $updatedAt: String $connect: String  $imports : String)
{
  updateComponent(input: {
    id: $id 
    name: $name 
    connect:  $connect
     imports: $imports
    description:$description 
    updatedAt:$updatedAt
  })
  {
      id
      name
      description
      connect
      imports
      type
  }
}`);

export const MutationCreateComponent = gql(`
mutation(
  $imports: String
  $connect: String
  $default: String
  $description: String
  $name: String!
  $parentId: String
  $privacy: String!
  $projectId: String!
  $referenceId: String!
  $type: String!
  $createdAt: String!) {

  createComponent( input: {
    imports: $imports
    connect: $connect
    default: $default
    description: $description
    name: $name
    parentId: $parentId
    privacy: $privacy
    projectId: $projectId
    referenceId: $referenceId
    type: $type
    updatedAt: $createdAt
    createdAt: $createdAt
   }) {
       id
      name
      description
      referenceId
      projectId
      type
  }
}`);



export const MutationDeleteComponent = gql(`
mutation($id: ID!) {
  deleteComponent(input: {id:$id}) 
  {
    id
  }
}
`);


export const MutationCreateComponent1 = gql(`
mutation{
  createComponent(input:{
  referenceId:"dcd496e7-3dba-4bdb-9964-59442e4d0da6"
  projectId: "dcd496e7-3dba-4bdb-9964-59442e4d0da6"
  name: "Cuero"
  type: "view"
  createdAt:"1"
  updatedAt:"1"
  privacy:"public"
  }){
    
    id
    name
    referenceId
  }
}

`)