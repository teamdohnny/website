import { graphql } from "react-apollo";
import gql from "graphql-tag";

import {
    MutationUpdateComponent, 
    MutationCreateComponent, 
    MutationDeleteComponent
} from "../Queries/mutations"

import {
    QueryGetProject, QueryGetProjectWithComponentNameList
} from "../../Projects/Queries/Fetch"

import {
    QueryGetComponent, getComponentOnComponentFilter
} from "../Queries/fetch"

//const uuidv4 = require('uuid/v4');

/*
This Mutation delete a component and update the local cache data :D
*/

export const graphQueryDeleteComponent = graphql(
        MutationDeleteComponent,
        {
            options: props=>({
             
                updated: (proxy, { data: { deleteComponent } }) => {
                        /*
                    const query = getComponentOnComponentFilter;
                    
                    const variables = { referenceId: props.project.id, type: props.type };


                   
                    const data = proxy.readQuery({ 
                        query, 
                        variables 
                    });


                     console.log("pre",data.getComponentoComponentFilter.items)


                    data.getComponentoComponentFilter.items = data
                        .getComponentoComponentFilter
                                .items.filter(c => c.id !== deleteComponent.id);



                     console.log("after",data.getComponentoComponentFilter.items)


                    proxy.writeQuery({ query, data });*/
                }
            }),

            props: (props) => ({
                deleteComponent: (component) => {
                    return props.mutate({
                        variables: {

                            id: component.id 
                        },
                       // refetchQueries:[getComponentOnComponentFilter]
                    });
                }
            })
        })

//Update component
export const graphQueryUpdateComponent = graphql(
    MutationUpdateComponent,
        {
            props: (props) => ({
                updateComponent: (component) => {
                    return props.mutate({
                        variables: {...component },
                        optimisticRessponse: () => ({
                            updateComponent: {
                               ...component,  __typename: 'Component' 
                            }
                        }),
                    });
                }
            })
        }
    );

//Delete component:

export const graphQueryDeleteComponentInComponent = graphql(
        MutationDeleteComponent,
        {
            options: props=>({
                update: (proxy, { data: { deleteComponent } }) => {
                    
                    //const query = QueryGetProject;
                    const query = QueryGetComponent;
                    const variables = { id: props.referenceId };
                    const data = proxy.readQuery({ query, variables });
                    data.getComponent.components.items = data.getComponent.components.items.filter(c => c.id !== deleteComponent.id);
                    proxy.writeQuery({ query, data });
                }
            }),

            props: (props) => ({
                deleteComponent: (component) => {
                    return props.mutate({
                        variables: {
                            id: component.id 
                        }
                    });
                }
            })
        })

/*
==================================================================================
==================================================================================
==================================================================================


From here everything is a mess


==================================================================================
==================================================================================
==================================================================================

*/

//Update remove component inside componets
export const helpQueryDeleteComponentInComponent = {
    query:MutationDeleteComponent,
    "props":"component",
    "update-props":{ 
        id: "props.match.params.id",
    },
    response:{
    },
    optimisticResponse: true,
}




//Maybe aren´t required those elements====================================================================


const createConf =   {
            options: props => ({
            //refetchQueries: [{ query: QueryGetComponent }],
            update: (proxy, { data: { createComponent }, data }) => {
             
    

                const variables = { id: createComponent.referenceId};
                const isProjectQuery = createComponent.referenceId === createComponent.projectId


               const query = isProjectQuery? QueryGetProject: QueryGetComponent;
                 const referenceObj = isProjectQuery? "getProject": "getComponent";


                const response = proxy.readQuery({ query, variables });

                    response[referenceObj] = { 
                        ...response[referenceObj],
                        components: {
                            ...response[referenceObj].components,
                            items: [
                                ...response[referenceObj].components.items.filter(c => c.id !== createComponent.id),
                                createComponent,
                            ]
                        }
                    };
            

                proxy.writeQuery({ query, data: response });
            },
        }),
            props: (props) => ({
                createComponent: (component) => {
                    return props.mutate({
                        variables: {...component},
                    });
                }
            })
        }


const createNoUIConf =   {
            props: (props) => ({
                createNoUIComponent: (component) => {
                    return props.mutate({
                        variables: {...component},
                    });
                }
            })
        }

//======================== Any component uses this I think :)
export const createComponent = graphql(
    MutationCreateComponent,
    {
            options: props => ({
            update: (proxy, { data: { createComponent } }) => {

                const query = QueryGetProject;

                //console.log(createComponent.projectId)
                //console.log("ADDING ON ", createComponent.referenceId);

                const variables = { id: createComponent.referenceId};
                const data = proxy.readQuery({ query, variables });

                data.getProject = { 

                    ...data.getProject,

                    components: {

                        ...data.getProject.components,

                        items: [

                            ...data.getProject
                                    .components
                                        .items
                                            .filter(c => c.id !== createComponent.id),
                            createComponent,
                        ]
                    }
                };

                proxy.writeQuery({ query, data });
            },
        }),
            props: (props) => ({
                createComponent: (component) => {
                    return props.mutate({
                        variables: {...component},
                    });
                }
            })
        })

//==============================


//Update component Optimistic UI
export const graphQueryUpdateComponent2 = graphql(
        MutationUpdateComponent,
        {
            props: (props) => ({
                updateComponent: (component) => {
                    return props.mutate({
                        variables: {...component },
                    });
                }
            })
        }
    );

export const graphQueryCreateNoUIComponent = graphql(MutationCreateComponent, createNoUIConf)

export const graphQueryCreateComponent = graphql(MutationCreateComponent, createConf)