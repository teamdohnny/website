import { graphql } from "react-apollo";

import {QueryGetComponent, QueryGetFullComponent, getComponentOnComponentFilter,getComponentOnComponent} from "../Queries/fetch"

export const graphQueryReadComponent = graphql(
        QueryGetComponent,
        {

            //Use this in case is not a rute FetchbyComponentId"

            options: ({ match: { params: { id } } }) => ({
                variables: { id },
                fetchPolicy: 'cache-and-network',
            }),

            props: ({ data: { getComponent: component }, data, loading }) => {
               return ({
                    component,
                    loading: data.loading,
                })
           },
        },
    );


export const graphQueryReadComponentsFilter = graphql(
        getComponentOnComponentFilter,
        {

            //Use this in case is not a rute FetchbyComponentId"

            options: ({ referenceId, type }) => ({
                variables: { referenceId, type },
                fetchPolicy: 'cache-and-network',
            }),

            props: ({ data: { getComponentoComponentFilter:{items = []} = {items:[]}}, data, loading }) => {
              
               return ({
                    items: items,
                    loading: data.loading,
                    data
                })
           },
        },
    );
export const graphQueryReadComponentsOnReference = graphql(
        getComponentOnComponent,
        {

            //Use this in case is not a rute FetchbyComponentId"

            options: ({ referenceId, type }) => ({
                variables: { referenceId, type },
                fetchPolicy: 'cache-and-network',
            }),

            props: ({ data: { getComponentOnComponent:{items = []} = {items:[]}}, data, loading }) => {
            
                
               return ({
                    items: items,
                    loading: data.loading,
                    data
                })
           },
        },
    );


/*
==================================================================================
==================================================================================
==================================================================================

=================  From here everything is a mess   ==============================

==================================================================================
==================================================================================
==================================================================================
*/


//cache-only

export const graphQueryReadFullComponent2 = graphql(
        QueryGetFullComponent,
        {
            options: ({ match: { params: { id } } }) => ({
                variables: { id },
                fetchPolicy: 'cache-and-network',
            }),
            props: ({ data: { getComponent: fullComponent }, data, loading }) => ({
                fullComponent,// Convert into an array of components <->
                loading:data.loading,
            }),
        },
    )

export const graphQueryReadFullComponent = graphql(
        QueryGetFullComponent,
        {
            options: ({ component }) => ({
                variables: { id:component.id },
                //fetchPolicy: 'cache-and-network',
            }),
            props: ({ data: { getComponent: fullComponent }, data, loading }) => {
               // console.log("Si te gusta el reggeton DALE!", data)
                return({
                            fullComponent,// Convert into an array of components
                            loadingComponent:data.loading,     
                        })},
        },
    )

export const graphQueryReadFullComponentXray = graphql(
        QueryGetFullComponent,
        {
            options: ({ id }) => ({
                variables: { id },
                fetchPolicy: 'cache-and-network',
            }),
            props: ({ data: { getComponent: fullComponent }, data, loading }) => ({
                fullComponent,// Convert into an array of components
                loading:data.loading,     
            }),
        },
    )


/*
    Think how to fix...

*/
export const graphQueryReadFullComponentXrayCached = graphql(
        QueryGetFullComponent,
        {
            options: ({ id }) => ({
                variables: { id },
                fetchPolicy: 'cache-only',
            }),
            props: ({ data: { getComponent: fullComponent }, data, loading }) => ({
                fullComponent,// Convert into an array of components
                loadingCachedData:data.loading,     
            }),
        },
    )


export const graphQueryReadFullComponentXrayCache2 = graphql(
        QueryGetFullComponent,
        {
            options: ({ parentId, data }) => {
                //console.log(data)

                return({
                            variables: { id: parentId },
                            fetchPolicy: 'cache-and-network',
                        })},
            props: ({ data: { getComponent: fullComponent }, data, loading }) => {
                  //  console.log("Yeeeeees",fullComponent)
                return({
                            parentComponent: fullComponent,// Convert into an array of components
                            loadingCachedParentComponent:data.loading,     
                        })},
        },
    )

//=============================================


//=================

export const graphQueryReadComponent2 = graphql(
        QueryGetComponent,
        {
            options: ({ component_id }) => ({
                variables: { id:component_id 
                },
            }),
            props: ({ data: { getComponent: component }, data, loading }) => {
               // console.log("this happend",component)
               return ({
                component,
                data_fetch:data,
                loading:data.loading,
            })},
        },
    )

//=================