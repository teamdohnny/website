import styled , { keyframes } from 'styled-components' ;
export const float = keyframes`

 0% {
    box-shadow: 0 3px 15px 0px rgba(0,0,0,0.6);
    transform: translatey(0px);
  }

  50% {
    box-shadow: 0 9px 15px 0px rgba(0,0,0,0.2);
    transform: translatey(-4px);
  }

  100% {
    box-shadow: 0 3px 15px 0px rgba(0,0,0,0.6);
    transform: translatey(0px);
  }

`;

//Add your code here!
 const BlikBlink = keyframes`

  0%{
    background: red;
  }
   25%{
    background: #0035C9;
  }
  50%{
    background: green;
  }
   75%{
    background: yellow;
  }
  100%{ 
    background: white;
  }

`;
 
const Body = styled.div`
  animation: ${BlikBlink} 0.0000001s linear infinite;
  width: 100%;
  height: 100vh;
  background: #0035C9;
`;

//render(<Body>I dont like this ungly feature</Body>)