import styled from 'styled-components';
import React from 'react';
import BaselineArrowForward from "react-md-icon/dist/BaselineArrowForward";
import RoundPlayCircleFilledWhite from "react-md-icon/dist/RoundPlayCircleFilledWhite";
import {Container, Text} from 'local_npm/clay-components';
import {Input as BaseInput} from "local_npm/react-container"



const Tag = styled.label`
    position: absolute;
    color:${props=>props.theme.colors.green};
    background: white;
    padding:0 0.1rem;
    border-radius:2px;
    font-size: 12px;
    font-weight: 500;
    top: -1px;
    left: 0.7rem;
`;

export const Button = (props)=>( <Container

    
      {...props}

      css={`

         /*====Start Default Button styles====*/
        position: relative;
        margin: 0;
        overflow: hidden;
        cursor: pointer;
        font-weight: 500;
        font-size: 14px;
        border: 0; 
        /*====Finsih Default Button styles====*/
        ${props.css && props.css}

        ${props.disabled && "opacity: 0.2; cursor: not-allowed;"}


        `}



       as="button"/>)




export const Input = (props)=> {

  const [focus, setFocus] =React.useState(false)



  const palette = props.light?{
    primary:"white",
    secondary:"#212121",
    textColor1:"#212121",
    textColor2:"white",

  } : {
    primary:"transparent",
    secondary:"white",
    textColor1:"white",
    textColor2:"#212121",

  } 
      
    return (
    <Container 
      overflowHidden

       css={` 
         
        
          border-radius: 5px;
          position: relative;
          
          
          border: 1px solid ${palette["secondary"]};  
          height: 41px;
          margin: 0.5rem auto;
        `}


        tabletCSS={` 


         ${props.responsive && `border: none;
                            height: auto;`}


          `} 

    >
    <Container row>
    <Container css={`

 width: 65%;

      `}


       tabletCSS={` 


         ${props.responsive && `
            border-radius: 5px;
            width: 100%;
            border: 1px solid ${palette["secondary"]}; 
            margin: 1rem auto;
            

          `}


          `} 




      >
 
        <BaseInput
        {...props}
        css={` 

          padding-left: 0.5rem;
          font-size: 14px;
         height: 41px;
        width: 100%;


         
          position: relative;
          background:  ${palette["primary"]};
          color: ${palette["textColor1"]};;
          border: none;  

          &:focus {
            outline: none !important;
             border: none;  
           
          }
       
        `} 


         tabletCSS={` 


         ${props.responsive && `
           
            

          `}


          `} 

        
        
        onBlur = {(e)=>{
          setFocus(false)
          if(props.onBlur)
            props.onBlur(e)


        }}

        onFocus = {(e)=>{
          setFocus(true);
            if(props.onFocus)
            props.onFocus(e)
        }}


        
   /> 
        
</Container>
<Container     

  css={`
        width:35%; 
        height: 41px;
        background: ${palette["secondary"]};
        color: ${palette["textColor2"]};
  `} 

  tabletCSS={`
   ${props.responsive && `
      width: 100%;
      border: none;
    `}
    `}>
     
        <Button 

          onClick={props.submitClick}
          css={`
            width:100%; 
            border-radius: 5px;
            height: 41px;
            background: ${palette["secondary"]};
            color: ${palette["textColor2"]};
            `}  
            height="100%"{...props.button}>

          {props.submit?props.submit:"Submit"}
        </Button>
        </Container>
         
        </Container>



    {/*(focus || props.value) && <Tag>{props.placeholder}</Tag>*/ }

    </Container>)   



  
}
 









export const ArrowIcon = styled(BaselineArrowForward) `
  color: blue;
`

export const PlayIcon = styled(RoundPlayCircleFilledWhite) `
  color: ${props => props.theme.secondaryBlue};
  font-size: 20px;
`
