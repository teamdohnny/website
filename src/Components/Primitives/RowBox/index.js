import React, {useState} from "react"
import {Container, Text} from 'local_npm/react-container';
//import uuid from "uuid/v4";







const Section = (props)=>{
  console.log(props)
  return (
    <Container
        
        css={`
          display: inline-flex;
          align-items: flex-start;
          justify-content: center;
          height: 100%;
          width: auto;
          ${!props.nowrap &&  "white-space: nowrap;"}
          ${props.start === true && `
              justify-content: flex-start; 
                align-items: start;
          `}

          ${props.center === true && `
              justify-content: flex-center; 
                align-items: center;
              
            `}

          ${props.end === true && `


              justify-content: flex-end; 
                align-items: end;  
          
           `}

      
        `}


          width="auto"
          {...props}
          
          height="100vh">
        {props.children}
      </Container>)
}


export const Item = (props)=>{

  return (<Container

            
         
            css={`
                  padding:0 1rem;
                `}


            desktopCSS={`

                ${props.hideBig && `display: none;`}
              
              `}  


            tabletCSS={` 


               ${props.hideBig && `display: inline-block;`}
               ${props.hideSmall && ` display: none; `}
              `}

            height="100%"
            width="auto"
            center
            {...props}>
            {props.children}
          </Container>)
}


export const RowBox = (props)=> {

  
  const {
    children, 
    top="0", 
    start = [], 
    center = [], 
    end = []
  } = props;




  //This should use the wrapper prop;

  return (
      <Container
        background="white"
        height="60px"
        css={"padding: 0; margin:0;"}
        {...props}
      >

        <Container 
        
          height="100%"
          row >

         {start.length > 0 &&
          <Section 
            
           
            start>{ start.map((item, key)=>item)}
          </Section> 
          }


          {center.length > 0 &&
          <Section 
            
           
            center >{ center.map((item, key)=>item)}
          </Section> 
          }

          {end.length > 0 &&
          <Section end>{ end.map((item, key)=>item)}
          </Section> 
          }
          {children}
        </Container>
      </Container>)
}

export default RowBox





/*
As Navbar

<RowBox 
  as="nav"
  position= "fixed"
  distance ={ `${top} 0 auto 0`}>


*/