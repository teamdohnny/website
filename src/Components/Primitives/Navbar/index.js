import React from "react"
import {Container, Text} from 'local_npm/react-master-component';


const Section = (props)=>{
  return (<Container
        css={`
          display: inline-flex;

          align-items: flex-start;

          ${props.start && `justify-content: flex-start;`}
          ${props.start && `justify-content: flex-center;`}
          ${props.start && `justify-content: flex-end;`}

          justify-content: center;
          min-width: 0;
          height: 100%;
          white-space: nowrap;


          `}
        width="auto"
        {...props}
        center
        height="100vh">
        {props.children}
      </Container>)
}


export const Item = (props)=>{
  return (<Container
        width="auto"
        {...props}
        center
        height="100vh">
        {props.children}
      </Container>)
} 


export const RowBox = (props)=> {
  const {children, top="0"} = props
  //This should use the wrapper prop.
  return (<Container
            defaultShadow
            defaultCorner
            center
            background="white"
            {...props}
            width = "100%"
            height="60px"
            margin="2rem 0"
            row 
            css={"padding: 0 1rem;"}
            >
        <Container>
        {children}
        </Container>
      </Container>)
  /*
       

  */
}


/*
<RowBox 
  as="nav"
  position= "fixed"
  distance ={ `${top} 0 auto 0`}>


*/