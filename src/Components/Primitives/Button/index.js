import styled from 'styled-components';
import BaselineArrowForward from "react-md-icon/dist/SharpArrowForward";
import RoundPlayCircleFilledWhite from "react-md-icon/dist/SharpPlayCircleFilledWhite";
//#0035C9

export const Button = styled.button`
  position: relative;
 
  margin: 0;
  border-radius: 3px;
  overflow: hidden;
  background: ${props => (!props.transparent ? "#0035C9" : "transparent")};
  color: ${props => (!props.transparent ? "white" : "#0035C9")};
  cursor: pointer;
  font-weight: 500;
  font-size: 18px;
  border: 0;
  ${props=>props.center && `
  		${props.theme.utils.centerContent()}
  		${props.theme.utils.rowContent()}
  		  height: 40px;
  		  margin: 0 auto;
        text-aling: left;
  	`}
`;

export const Button1 = styled.button `
	background: ${props => props.theme.secondaryBlue};
	width: 105px;
	height: 34px;
	border: none;
	border-radius: 3px;
	color: ${props => props.theme.main};
	font-size: 14px;
	cursor: pointer;
	font-weight: ${props => props.theme.fontWeight.regular};
	cursor: pointer;
	position: relative;
	${(props) => props.theme.media.tablet `
	`}
`

export const ArrowIcon = styled(BaselineArrowForward) `
	color: #0035C9;
`

export const PlayIcon = styled(RoundPlayCircleFilledWhite) `
	color: ${props => props.theme.secondaryBlue};
	font-size: 20px;
`
