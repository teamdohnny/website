import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';

const Nav = styled.nav`
    top:0;
    left:0;
    position: fixed;
    overflow: hidden;
    width: 100%;
    background: white;
    color: ${props=>props.theme.color.navbarText};
    height: 60px;
    ${props=>props.theme.utils.rowContent}
    z-index:${props=>props.help?"9999":"1"};   
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: 100%;
    margin: 0 20px;
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
`;


 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
`;

 const Item = styled.div`
    position: relative; 
    height: 60px;
    cursor: pointer;
    min-width: 45px;
    color:${props=> props.active?"black":"#5F6469" };
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 0.5rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 700px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}
    ${props=>props.hide && `
        @media (max-width: 700px)
        {
          display: none;
        }
    `} 
    a{
        text-decoration: none;
         color:${props=> props.active?"black":"#5F6469" };
 
       font-weight: 500;
    }
`;

const TabSet = styled.div`
  display: ${props=>props.notNavbar?"none": "contents"};
  @media (max-width: 700px) {
    display: none;
  }
`;

const TabItem = ({children, active, onClick})=>{      
  const TabSelector = styled.div`
    background: ${(props)=>props.theme.colors.green};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 16px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
    margin:0 auto;
  `;
  return (
    <Item 
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer>
      
      {children}
      
      {active &&  <TabSelector/>}

    </Item>
    )
}