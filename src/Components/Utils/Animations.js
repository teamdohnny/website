import React from 'react';
import {Container} from 'local_npm/clay-components';
import withReveal from 'react-reveal/withReveal';
import Bounce from 'react-reveal/Bounce';


export const withRevealAnimationContainer = (AnimationComponent, animationProps={cascade: true, bottom: true} )=> {

if(!AnimationComponent)
	AnimationComponent = Bounce;

	const Animation = (props)=> {
	    const {delay} = props
	    
	    const TMP = withReveal(Container,
	      <AnimationComponent
	        {...animationProps}
	        delay={delay} 
	      />)

	    return <TMP {...props}/>
	  }

	return Animation
}