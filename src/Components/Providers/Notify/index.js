import React, { Component } from 'react';
import NotificationAlert from "react-notification-alert";
export const NotifyContext = React.createContext();

const _ = require('lodash');


//Component Did mount shoud fetch products.
export default class NotifyProviders extends React.Component
{
    constructor()
    {
      super()
      this.state = {
        notifications: [] 
      }
        
    }

    componentDidMount(){

      if(window.server_data.error)
        this.notify({text:window.server_data.error, type:"danger"})

      
       if(window.server_data.softError)
        this.notify({text:window.server_data.softError, type:"danger"})
      
    }


    notify(notification){

        const options = {
          place: "tr",
          message: notification.text,
          type: notification.type,
          icon: "tim-icons icon-alert-circle-exc",
          autoDismiss: 18
        };

        this.refs.notificationAlert.notificationAlert(options);


      }


  
  render()
  {  
      
      return (
        <NotifyContext.Provider 
          value={{notify: this.notify.bind(this) }}>
          <React.Fragment>
                    {this.props.children}
          </React.Fragment>
        <div className="rna-container">
          <NotificationAlert ref="notificationAlert" />
        </div>
        </NotifyContext.Provider>
      );
    }
}

export const withNotify = (WrapperedComponent)=>
{
  return (props)=>(
            <NotifyContext.Consumer>      
             {(rest)=>(
                <WrapperedComponent {...rest} {...props}/>
              )}
            </NotifyContext.Consumer>
          )
  }