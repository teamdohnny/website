import React, {useState, useEffect} from 'react';
import axios from "axios";

export const useShooterGetList = ({params = {type:"story"}, omit = null , callback = null}) =>{

    const [items, setItems] = useState([])
    const [loadingItems, setLoading] = useState(true)
    const [itemsError, setError] = useState(false)

    useEffect(()=>{
         axios.get(`${process.env.REACT_APP_SHOOTER_API}/getPostList`,{params}).then((res)=>{

           let items2 = res.data.items;
          
            if(items2 && omit !== null)
            {
                items2  = items2.filter((val)=>{return val[omit.key] !== omit.value;});   
            }

            setItems(items2);
            setLoading(false);

        }).catch(()=>{
            setError(true)
          setLoading(false);

        })

    },[])

    return {items, loadingItems, itemsError}
}


export const useShooterGetItem = ({params = {}, callback = null}) =>{
    const [item, setItem] = useState({})
    const [loadingItem, setLoading] = useState(true)
    const [errorItem, setError] = useState(false)

    useEffect(()=>{

         axios.get(`${process.env.REACT_APP_SHOOTER_API}/getPost`,{params}).then((res)=>{

           
              setItem( res.data.item );
              setLoading(false)

        }).catch(()=>{

          setError(true); 
          setLoading(false);

        })


    },[])

    return {item, loadingItem, errorItem}
}