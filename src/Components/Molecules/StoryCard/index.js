import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import OutlinePersonAdd from "react-md-icon/dist/SharpAdd";
import {Link} from "react-router-dom"


import Bounce from 'react-reveal/Bounce';
import {withRevealAnimationContainer} from "Components/Utils/Animations"




const AnimationContainer = withRevealAnimationContainer(Bounce);

const Landing = ({
cover="https://cdn.dribbble.com/users/2587756/screenshots/6787212/group_14013_2x_2x.png",
titulo="Shooter",
subtitulo="Series of visual concepts for improving the user experience.", 
mapIndex=20,
item = {}
}) => {

/*
 <Animation 
           delay={(key + 1)*50} >

*/
   

  return (
    
   <React.Fragment>
      <Container
        width="50%" 
        css="padding:1rem"
         tabletCSS={`
                   margin: 0 auto;
                   width: 100%;
                   padding:1rem 0;
                 `}
       
        >
      <AnimationContainer
        delay={(mapIndex + 1) * 50}
        defaultShadow
        defaultCorner
        overflowHidden
        css="cursor:pointer;"
        background="#F8F9FA">
      
      <Link to={"/story/" +  item.id} style={{textDecoration:"none"}}>

  

            <Container

                overflowHidden
                width="100%"
                height="300px"

                 tabletCSS={`height: 200px;`}
                margin="0rem 0"
                backgroundImg={`"${cover}"`} />


        <Container css="padding:2rem;" tabletCSS={`padding:1rem;`}>
          <Typography  
            css="line-height:55px;" 
            color="#212121" 
            weight="600" fontSize="45px" marginB="30"
            tabletCSS={`line-height:30px; font-size: 20px; margin-bottom:10px;`}

            >
          {titulo}
          </Typography>
          <Typography  css="line-height:26px;"  tabletCSS={`line-height:24px; font-size: 14px;`} color="#212121" fontSize="16px" marginB="12">
          {subtitulo}
          </Typography> 
        </Container> 
</Link>
</AnimationContainer>

      </Container>



    </React.Fragment>
)};

export default Landing;