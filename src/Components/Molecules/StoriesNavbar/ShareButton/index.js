import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import SharpShare from "react-md-icon/dist/SharpShare";
import SharpArrowBack from "react-md-icon/dist/SharpArrowBack";
import {Link} from "react-router-dom"
import ReturnButton from "Components/Molecules/ReturnButton"
import Fade from 'react-reveal/Fade';
import _ from "lodash"
import SharpClose from "react-md-icon/dist/SharpClose";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
   FacebookIcon,
  TwitterIcon,
  TelegramIcon,
  WhatsappIcon,
  LinkedinIcon,
} from 'react-share';



const ReactMarkdown = require('react-markdown')



const Share = (id="")=>{


  const [open, setOpen] =  React.useState(false)






    return(
      <React.Fragment>
        <Container
        onClick={()=>{setOpen(!open)}} 
        as="button"
        circle="40px" 
        center
        border="1px solid rgba(0,0,0,0.1)"
        background="white"
        tabletCSS={`
          border: none;

          `}
        css={`
        cursor: pointer;
        &:hover{

        background: rgba(0,0,0,0.1);
        }

        `} >
      {open?<SharpClose style={{color:"#677282", fontSize:"24px"}}/>:<SharpShare style={{color:"#677282", fontSize:"24px"}}/>}
      </Container>


      {open && 


        <Container
            
            width="180px"
            defaultCorner
            background="white" 
            shadow="0 0.5px 3px rgba(0,0,0,0.4)" 
        
             
            css={`

              position:absolute; 
              bottom:-210px; 
              right: 20%;
              cursor: pointer;
              &:hover{
                box-shadow:0 0.3px 8px rgba(0,0,0,0.5);

              }
              padding: 1.2rem;


              `} >


              <Container row css="position: relative; margin-bottom: 30px; ">

              <Typography 
                marginT="2"
                weight="600"
                fontSize="16px"
                color="#202124">
                Compartir
              
              </Typography>
                

              </Container>

              <FacebookShareButton url={process.env.REACT_APP_STORY_URL+"/"+id}>
              <Container row width="80%"  margin="1rem 0">
                <Container width="20%"  height="30px" center>
                <Img src={require("static/imgs/Iconos/Redes Sociales/Facebook.svg")}/>
                </Container>
                <Container width="80%" height="30px" center>
                <Container css="padding-left: 0.7rem;">
                  <Typography color="#1877F2" fontSize="14px" weight="500"  >Facebook</Typography>
                 </Container>
                </Container>
              </Container>
               </FacebookShareButton>

               <TwitterShareButton url={process.env.REACT_APP_STORY_URL+"/"+id}>
             <Container row width="80%"  margin="1rem 0">
                <Container width="20%"  height="30px" center>
                <Img src={require("static/imgs/Iconos/Redes Sociales/twitter.svg")}/>
                </Container>
                <Container width="80%" height="30px" center>
                <Container css="padding-left: 0.7rem;">
                  <Typography align="left" color="#55ACEE" fontSize="14px" weight="500"  >Twitter</Typography>
                  </Container>
                </Container>
              </Container>
               </TwitterShareButton>


               <LinkedinShareButton url={process.env.REACT_APP_STORY_URL+"/"+id}>
              <Container row width="80%"  margin="1rem 0">
                <Container width="20%"  height="30px" center>
                <Img src={require("static/imgs/Iconos/Redes Sociales/linkedin.svg")}/>
                </Container>
                <Container width="80%" height="30px" center>
                   <Container css="padding-left: 0.7rem;">
                  <Typography color="#007AB9" fontSize="14px" weight="500"  >Linkedin</Typography>
                  </Container>
                </Container>
              </Container>
               </LinkedinShareButton>



       

              </Container>




      }

      </React.Fragment>


              )



}

export default Share






