import React, {useContext}  from "react"
import Navbar, {Item} from "Components/Primitives/RowBox"; 
import {Container, Text} from "local_npm/clay-components"
import {Img} from "local_npm/react-container"
import {Link} from "react-router-dom"
import {ModalContexts} from "Components/Providers/Modals"
import BlogSubscriptionModal from "Components/Molecules/BlogSubscriptionModal"
import ShareButton from "./ShareButton"
import ArrowIcon from "react-md-icon/dist/SharpArrowBack";
import uuid from "uuid/v4"
import IconShare from "react-md-icon/dist/SharpShare";
const logo = require("static/imgs/Logos-MKT/Logo da.png");

const BlogNavbar = ({id})=>{

	const {openModal, closeModal} = useContext(ModalContexts);

	const openSubscribeModal = ()=> {
		openModal(<BlogSubscriptionModal/>,true)

	}
 
	return (
		<Navbar 
		  	as="nav"
		  	defaultShadow
			
			height="65px"
			width="90%"

			margin="0 auto"
			wrapper={{position:"fixed", css:"z-index: 2;"}}
			css={`
				padding: 0 3rem;
        	max-width: 1236px;
				z-index: 2;
			`}
			tabletCSS={`
					padding: 0 1rem;
				top: 64px;
				`}

			start={[

				<Item tabletCSS="display: none;" css={`padding: 0; padding-right: 1rem;`} key={uuid()}>
					<Link to={"/"}>
	                	<Img src={logo} width="38px"/>
	              	</Link>	
				</Item>,
				<Item  css="display: none;" tabletCSS="display: flex;"  key={uuid()}>
					<Link to={"/stories"}>
	                	<ArrowIcon style={{color:"#677282", fontSize:"30px"}}/>
	              	</Link>	
				</Item>
			]}


			center={[
				<Item key={uuid()}>
					<Text color="#202124" fontSize="20px" weight="600">
					Casos de éxito.
					</Text>
				</Item> 
				]}


			end={[
				<Item css={"padding:0;"} key={uuid()}>

				<ShareButton id={id}/>
					

				</Item>]}
		/>
		

		)
}

export default BlogNavbar