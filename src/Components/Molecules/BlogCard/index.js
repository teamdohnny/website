import React from 'react';
import styled from 'styled-components';
import BaselineArrowForward from "react-md-icon/dist/BaselineArrowForward";
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Text} from "local_npm/clay-components"
import {Link} from "react-router-dom"
import {withRevealAnimationContainer} from "Components/Utils/Animations"
import Bounce from 'react-reveal/Bounce';
import {Button} from "Components/Primitives/Button"

const AnimationContainer = withRevealAnimationContainer(Bounce);

const BlogCard = (props) => {


 // console.log(props)

  const {
    cover="https://cdn.dribbble.com/users/2587756/screenshots/6787212/group_14013_2x_2x.png",
    titulo="El proceso de diseño de nuestra hermosa marca.",
    subtitulo=`Te contamos como fue que ideamos y diseñamos la marca "da." y aprende que cosas puedes implementar para diseñar la tuya. Usamos memes.`, 
    mapIndex=20,
    item = {}
  } = props;

  return (    
   <React.Fragment>
     <AnimationContainer
      delay={(mapIndex + 1) * 50}
      defaultShadow
      defaultCorner
      overflowHidden
      margin="1rem auto"
      css={`
        cursor:pointer;
        padding: 2rem;
   
        `}
        tabletCSS="padding: 1rem;"
      background="#F8F9FA"
      
      > 

    <Link to={"/post/" +  item.id} style={{textDecoration:"none"}}>
      <Container row>
         <Container  width="50%"   height="300px" css="margin: 2rem 0;"
         tabletCSS={`

          height: auto;
          padding: 2rem 0;
          width: 100%;

          `} >
           <Typography  color="#212121" weight="600" fontSize="35px" marginB="30" css="line-height: 45px;"
           tabletCSS={`font-size: 20px; line-height: 30px;`}

           >
               {titulo}
           </Typography>
           <Typography   color="#212121" fontSize="18px" marginB="12" css="line-height: 28px;"   tabletCSS={`font-size: 14px; line-height: 24px;`}
>
               {subtitulo}
           </Typography>

<Container css={`position: absolute; bottom:0;`}tabletCSS={`display: none; `}>
            <Button 
                

                transparent style={{position: "absolute", bottom:"0rem", left:"0rem", color:"#202124", marginLeft:"-0.5rem"}}> 
                <Container 
                center 
                row 
                style={{fontWeight:"500"}}>
               

                <Text 
                fontSize="16px"
                weight="500" 
                align="left" 
                marginR="15"
                > 
                Leer publicación
                </Text> 
                 <BaselineArrowForward style={{fontSize:"24px"}}  />

                </Container>
                </Button>
                </Container>




         </Container>
          <Container center width="50%"  height="348px" tabletCSS={`width: 100%;`} >
             <Container
                defaultShadow
                corner="10px"
                overflowHidden
                tabletCSS={`width: 100%; height: 175px; margin-bottom: 80px;`} 
                width="80%"
                height="348px"
                margin="0rem 0"
                backgroundImg={`"${cover}"`} />



                <Container css={`display: none;`} tabletCSS={`display: inline-block;`}>
            <Button 
                

                transparent style={{position: "absolute", bottom:"1rem", left:"0rem", color:"#202124", marginLeft:"-0.5rem"}}> 
                <Container 
                center 
                row 
                style={{fontWeight:"500"}}>
               

                <Text 
                fontSize="16px"
                weight="500" 
                align="left" 
                marginR="15"
                > 
                Leer publicación
                </Text> 
                 <BaselineArrowForward style={{fontSize:"24px"}}  />

                </Container>
                </Button>
                </Container>
          </Container>
        </Container>
      </Link>
     </AnimationContainer>

  </React.Fragment>
)};

export default BlogCard;



