import React, {useContext, useState} from 'react';
import styled from 'styled-components';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import InputSubmit from "Components/Molecules/ContactInput"
import {Input} from "./Input";
import Typography from 'local_npm/react-styled-typography';
import Slide from 'react-reveal/Slide';
import {Link} from "react-router-dom"
import SuccessCard from "./SuccessCard";
import axios from "axios"
import {ModalContexts} from "Components/Providers/Modals"
import Recaptcha from 'react-recaptcha';

const uuidv4 = require('uuid/v4');

export const Fullscreen = styled.div`
  ${props=>props.theme.utils.fullscreen()}

`;

export const Body = styled.div`
  width: 100%;
  margin: 0 auto;
  text-align: center;
  background: #202124;
  padding: 90px 0;

  position: relative;
  color: #202124;
  min-height: 100vh;

  ${props=>props.theme.media.tablet`
    padding-bottom: 200px;
    font-size: 45px;
      padding: 90px 0px;
  `}

`
export const Title = styled.div`
    font-size: 50px;
    color: #202124;
    font-weight: 500;
    width: 80%;
    margin: 0 auto;
    margin-top: 100px;
    
    text-align: center;
     ${props=>props.theme.media.tablet`
      width: 90%;
      font-size: 45px;
    `}
`
export const Subtitle = styled.div`
 font-size: 18px; 
 color: #65696B;
  width: 70%;
  text-align: center;
  margin: 2rem auto;
  line-height: 25px;
  font-weight: 400;
  ${props=>props.theme.media.tablet`

       line-height: 26px;
     font-size: 16px; 
      width: 80%;
  `}
`;



export const Row = styled.div`
  width: 90%;
  margin: 0 auto;
  ${props=>props.theme.utils.rowContent()}
`;


export const Form = styled.form`
position: relative;
  width: 90%;
  margin: 0 auto;
  padding-bottom: 100px;
   ${props=>props.theme.media.tablet`
      width: 100%;
  `}
`;

export const RowInput = styled.div`
  width: 100%;
  ${props=>props.theme.utils.rowContent()}
`;

  export const Item = styled.div`
    width: 49%;
     ${props=>props.theme.media.tablet`
     width: 100%;
  `}
 
`;


export const Button1 = styled.button`
  background: #0035C9;
  color: white;
  font-size: 18px;
  padding: 0.8rem 1.2rem;
  border: none;
width: 100%;


 ${props=>props.disabled && `
     background: gray;
     cursor: not-allowed;
  `}


   ${props=>props.theme.media.tablet`
     width: 100%;
     margin: 30px auto;
  `}

`;


const [img1, img2] =[ require("static/imgs/5. Contactanos/Contactanos web.svg"), require("static/imgs/5. Contactanos/Contactanos web 2.svg")]
const [img1m, img2m] =[ require("static/imgs/5. Contactanos/Contactanos Movil.svg"), require("static/imgs/5. Contactanos/Contactanos movil 2.svg")]
const EPModal = () => {

   const {closeModal, openModal} = useContext(ModalContexts);
   const Modal = ()=>{
    return <SuccessCard close={closeModal}/>
}




return(
<Container
      center
      height="470px"
      tabletCSS="height: 969px;"
      css={`
      width:100%; 
      margin: 0 auto;
      background: #202124;
  `}>




   <Img src={img1m}


       
        width="100%"
        tabletCSS="display: inline-block;"
        css={`
          display: none;
          transition: 0.4s;
          transition-timing-function: ease-out;
          position: absolute;
          top: 0px;
          left:-1rem;
          right:0rem;
          `} 

        />


  <Img src={img1}

        mediaCSS={[{size:"1150px", css:` width:310px; `}]}
        width="410px"
         tabletCSS="display: none;"
        css={`
           transition: 0.4s;
          transition-timing-function: ease-out;
          position: absolute;
          bottom: -3px;
          left:0;
          `}/>


<Container>



        <Container
          width="400px"         
          css={` 
            z-index:1;
            left: 200px;
            top: -50px;
          `}

          tabletCSS={`
            margin-top: 40px;
            margin: 0 auto;
            width:90%;
            left: auto;
            top: auto;
          `}>

        <Typography 
          fontSize="60px"
          weight="600"
          css="line-height: 70px; "
          marginB="40"
          align="left"  
          tabletCSS=" font-size: 45px; line-height:55px; width: 100%;"
          color="white"> Comunícate con nosotros. </Typography>

          <InputSubmit
            responsive
            submit="Hablemos"
            placeholder="Tu dirección de email"
           />
 
          </Container>

</Container>





 <Img src={img2m}


       
        width="100%"
        tabletCSS="display: inline-block;"
        css={`
          display: none;
          transition: 0.4s;
          transition-timing-function: ease-out;
          position: absolute;
          bottom: -80px;
          right:0rem;
          `} 

        />





     <Img src={img2}


        mediaCSS={[
          {size:"1150px", css:` width:310px; `},
          {size:"880px", css:` width:260px; `}
        ]}
        width="610px"
        tabletCSS="display: none;"
        css={`
          transition: 0.4s;
          transition-timing-function: ease-out;
          position: absolute;
          bottom: -3px;
          right:1rem;
          `} 

        />

</Container>
);
}

export default EPModal  


// onloadCallback={this.recaptchaLoaded}