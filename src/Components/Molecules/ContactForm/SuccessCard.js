import React, {useContext, useState} from 'react';
import styled from 'styled-components';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button";
import Typography from 'local_npm/react-styled-typography';






const SuccessCard = ({close}) => {


return(
<Container  background="white" width="100%"  css="min-height:100vh; overflow-y: scroll;">


<Container 
    row   
    width="70%"  
    css="padding-top:100px;"
    margin="0 auto" 
    tabletCSS={`
      padding-top: 20px;
      width:90%;
      margin: 0 auto;
    `}>

<Container 
  width="38%"  
  center 
  tabletCSS={`
    width:90%; 
    margin: 2rem auto;`}>
          <Img src={require("static/pictures/iconos/Enviado.png")} width="80%" tabletCSS="width: 60%;"/>

</Container>

<Container width="52%" tabletCSS={`width:90%; margin: 0 auto;`}>

    <Typography 
    align= "left"

     tabletCSS={`
          
            font-size:35px;
          `}
      
      fontSize="80px"
      weight="500"
      marginB="30" 
 
      >Enviado </Typography> 


      <Typography 
       align= "left"
      css="line-height: 32px;"
      tabletCSS={`
            line-height: 26px;
            font-size:16px;
          `}
      fontSize="22px"
      weight="400"
      marginB="30"  

      >Tu formulario fue enviado con éxito. En poco tiempo estaremos contactándote. </Typography> 




    <Button 
    onClick={close}
        transparent>
        <Container 
          left 
          row 
          style={{fontWeight:"500"}}>
          <Typography 
            weight="500" 
            align="left" 
            > Volver al inicio</Typography> 
            <ArrowIcon style={{marginLeft:"2rem"}}/>
        </Container>
      </Button>
  
  
</Container>

</Container>

</Container>
);
}

export default SuccessCard  


// onloadCallback={this.recaptchaLoaded}