import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"

import  Slide  from 'react-reveal/Slide';
 


const Landing = ({
  color="gray",
  Icon=ArrowIcon,
  title="Análisis de viabilidad",
  subtitle=` `,
}) => (
   <React.Fragment>
    <Slide  bottom>
    <Container 
      row 
      defaultBorder 
      margin="0 auto" 
      css={`
        margin-top:50px; 
        padding:2rem;
        border: 1px solid #212121;
      `}
           tabletCSS="padding:2rem 1rem;"


      >
     
      <Container 
        height="187px"
        width="10%" 
        center
        tabletCSS={`

              height: auto;
            display: inline-block;

            margin: 1rem auto;
            width:80%;
            text-align: left;
        `}>
      <Container  circle="62px" background={color} center>
        <Icon style={{color:"white", fontSize:"40px"}}/>
      </Container>

      </Container>

      <Container 
        height="187px"
        width="40%" 
        center
        tabletCSS={`
            height: auto;
          
            margin: 1rem auto;
            width:100%;
            text-align: center;
          `}>

        <Typography 
          fontSize="25px"
          weight="500"
         
          css={`width:80%; line-height:35px;`}
          tabletCSS={""} 
          color="#000">{title}</Typography> 

        
      </Container>
      <Container 
        height="187"
        width="50%" 
        center
        tabletCSS={`
           height: auto;
            
            margin: 1rem auto;
            width:100%;
            text-align: center;
          `}>

        <Typography 
          fontSize="18px"
          weight="400"
         
          css={`width:80%; line-height:28px;`}
          tabletCSS={""} 
          color="#000">{subtitle}</Typography> 

        
      </Container>

      

    </Container>
    </Slide>

    </React.Fragment>
);

export default Landing;