import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import Icon from "react-md-icon/dist/SharpArrowForward";
import {Link} from "react-router-dom"


import Bounce from 'react-reveal/Bounce';
import withReveal from 'react-reveal/withReveal';

import _ from "lodash"


const Animation = ({delay, children})=> {
    const TMP = withReveal (Container ,<Bounce cascade bottom delay={delay} /> )

    return <TMP> {children}</TMP>

}


const ServiceInfoCard = ({
img=require("static/imgs/2. Servicios/Consultoria.png"),
title="Consultorías y estrategias.",
features=[{
            title:"Auditoria UI/UX", 
            subtitle:`Entiende como puedes abordar los problemas desde la perspectiva de tus usuarios, identifica oportunidades y crea productos innovadores que sean escalables visualmente y fáciles de usar.`
            },
            {
            title:"Estrategia de producto", 
            subtitle:`Entiende como puedes abordar los problemas desde la perspectiva de tus usuarios, identifica oportunidades y crea productos innovadores que sean escalables visualmente y fáciles de usar.`
            },
            {
            title:"Metodologías agiles", 
            subtitle:`Entiende como puedes abordar los problemas desde la perspectiva de tus usuarios, identifica oportunidades y crea productos innovadores que sean escalables visualmente y fáciles de usar.`
}],
key=0}) => {





  return (
    
   <React.Fragment>
   
  
  

<Container {...{css:`

              padding:1rem
              margin: 0 auto;
              width: 90%;

             `,   
              tabletCSS:"width:100%;"
            }

           }>

              <Animation 
           delay={(key + 1)*50} >

       <Container
        corner="10px"
        overflowHidden
        tabletCSS={` padding:2rem 1rem;`}
        background="#F8F9FA"
        css={`
          padding:2.5rem;
          margin-bottom:50px;
          border: 1px solid #E3E4E7; 
          .icon{
            display:none;
          } 
          &:hover{
            
            .icon{
              display:inline-block;
          }}`}
       >
      
      

      <Container  row>

<Container width="40%" center   tabletCSS={`width:100%; margin-bottom: 30px;`} >
<Container width="90%" css="margin:0 auto;" tabletCSS={`width:100%;`}>
   <Typography  
      css="line-height:50px;" 
      color="#202124" 
      weight="600" 
      fontSize="40px" 
      tabletCSS={`font-size:25px; line-height: 35px;`}
      marginB="35">

       {title}
   </Typography>
   <Img src={img} width="80%"  tabletCSS={`margin: 0 auto;`}/>
   </Container>
</Container>
<Container width="60%" margin="1rem 0"  tabletCSS={`width:100%;`} >



{_.map(features, (item,key)=><Container key={key}>

     <Typography  
      css="line-height:30px;" 
      color="#202124" 
      weight="600" 
      fontSize="20px" 

      marginB="18">

       {item.title}
   </Typography>


      <Typography  
      css="line-height:26px;" 
      color="#202124" 
      weight="400" 
      fontSize="16px" 
      marginB="45">

       {item.subtitle}
   </Typography>

    


</Container>)}

 











</Container>



   </Container>
    </Container> 
   </Animation>

   </Container>


    </React.Fragment>
)};

export default ServiceInfoCard;