import React, {useContext}  from "react"
import Navbar, {Item} from "Components/Primitives/RowBox"; 
import {Container, Text} from "local_npm/clay-components"
import {Img} from "local_npm/react-container"
import {Link} from "react-router-dom"
import {ModalContexts} from "Components/Providers/Modals"
import BlogSubscriptionModal from "Components/Molecules/BlogSubscriptionModal"
import uuid from "uuid/v4"
import ArrowIcon from "react-md-icon/dist/SharpArrowBack";
import SubscribeIcon from "react-md-icon/dist/BaselineUnsubscribe"

const logo = require("static/imgs/Logos-MKT/Logo da.png");

const BlogNavbar = ({returnTo="/blog" })=>{

	const {openModal, closeModal} = useContext(ModalContexts);

	const openSubscribeModal = ()=> {
		openModal(<BlogSubscriptionModal/>,true)

	}

	return (
		<Navbar 
		  	as="nav"
		  	defaultShadow
			
			height="65px"
			width="90%"
			wrapper={{position:"fixed", css:"z-index: 2;"}}
			margin="0 auto"
			css={`
				padding: 0 3rem;
        	max-width: 1236px;
				z-index: 2;
			`}
			tabletCSS={`
				padding: 0 1rem;
				top: 64px;
				`}

			start={[

				<Item tabletCSS="display: none;" css={`padding: 0; padding-right: 1rem;`} key={uuid()}>
					<Link to={"/"}>
	                	<Img src={logo} width="38px"/>
	              	</Link>	
				</Item>,

				<Item  css="display: none;" tabletCSS="display: flex;"  key={uuid()}>
					<Link to={returnTo}>
	                	<ArrowIcon style={{color:"#677282", fontSize:"30px"}}/>
	              	</Link>	
				</Item>


				
			]}


			center={[
				<Item key={uuid()}>
					<Text color="#202124" fontSize="20px" weight="600">
					Keywords.
					</Text>
				</Item> 
				]}


			end={[
				<Item  tabletCSS="display: none;" css={`padding:0;`} key={uuid()}>
					<Container 
						onClick={openSubscribeModal}
						as="button" 
						center
						border="1px solid #202124"
						background="white"
						corner="5px"
						width="154px"
						height="41px"

						css={`
							cursor: pointer;
							&:hover{

								background: rgba(0,0,0,0.1);
							}

							`}>

						<Text color="#202124" fontSize="14px" >
							Suscríbete
						</Text>
					</Container>

				</Item>,



				<Item  css="display: none;" tabletCSS="display: flex;"  key={uuid()}  onClick={openSubscribeModal}>
					
	                	<SubscribeIcon style={{color:"#677282", fontSize:"30px"}}/>
	              
				</Item>

				]}
		/>
		

		)
}

export default BlogNavbar