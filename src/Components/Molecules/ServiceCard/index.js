import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/react-container';
import Icon from "react-md-icon/dist/SharpArrowForward";
import {Link} from "react-router-dom"
import Bounce from 'react-reveal/Bounce';
import withReveal from 'react-reveal/withReveal';




const Animation = ({delay, children})=> {
    const TMP = withReveal (Container ,<Bounce cascade bottom delay={delay} /> )

    return <TMP> {children}</TMP>

}


const ServiceCard = ({title="Estrategia de producto", delay=0}) => {

  return (
    
   <React.Fragment>
   
  
<Container {...{width:"33%", 
                  
                css:"padding:1rem",  tabletCSS:`
                padding:0.5rem 0;
               margin: 0 auto;
              
               width: 100%;
             `}}>

        <Animation 
          delay={(delay + 1)*50}>

       <Container
        corner="10px"
        overflowHidden
        background="#F8F9FA"
        css={`
          cursor:pointer; 
          border: 1px solid #E3E4E7; 
          .icon{display:none;} 
          &:hover{
            border: 1px solid black; 
            .icon{display:inline-block;
          }}`}>
      
      
<Link to="/services" style={{textDecoration:"none"}}>
   <Container css="padding:0 1rem;" row>

<Container width="80%" height="93px" center tabletCSS="height: 69px;">
<Container>
   <Typography  css="line-height:28px;" color="#202124" weight="500" fontSize="20px" tabletCSS="font-size:16px;">
       {title}
   </Typography>
   </Container>
</Container>
<Container width="20%" height="93px" center tabletCSS="height: 69px;">
  <Icon className="icon" style={{fontSize:"26px"}}/>
</Container>



   </Container>
   </Link>
    </Container> 
   </Animation>

   </Container>

  

    </React.Fragment>
)};

export default ServiceCard;