import styled from 'styled-components';


export const Logo = styled.img `
	heigth: 100%;
	width: ${100/7}%;
`

export const Img = styled.img `
	width:80%
	height: auto;
	margin: 2rem auto;
`

export const Content = styled.div `
	width 100%;

${(props) => props.theme.utils.rowContent()}
`

export const FixBar = styled.div `
	position:absolute;
	bottom: -10px;
	left:0;
	right: 0;
	height: 10px;
	background: white;
`

export const Container = styled.div `
	position: relative;
	margin-top: -35px;
	width: 100%;
	height: 105px;
	//box-shadow: 0 0.5px 5px rgba(0,0,0,0.5);
	background: white;	
	${(props) => props.theme.utils.centerContent()}
`;