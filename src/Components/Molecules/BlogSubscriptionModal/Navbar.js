import React, {useContext}  from "react"
import Navbar, {Item} from "Components/Primitives/RowBox"; 
import {Container, Text} from "local_npm/clay-components"
import IconClose from "react-md-icon/dist/SharpClose";
import {Link} from "react-router-dom"
import {ModalContexts} from "Components/Providers/Modals"


const logo = require("static/imgs/Logos-MKT/Logo da.png");

const BlogNavbar = ()=>{

	
	const {closeModal} = useContext(ModalContexts);
	return (
		<Navbar as="nav"
			background="transparent"
			width="100%"
			css={`
				padding: 0 1rem;
	
			`}

			start={[

				<Item key={"subs-1"}>
					<Text fontSize="20px" weight="600">
	                	Suscríbete a Keywords.
	              	</Text>	
				</Item>
			]}



			end={[
				<Item key={"subs-2"} onClick={closeModal}>
					<IconClose  style={{fontSize:"20px", cursor:"pointer"}}/>

				</Item>]}
		/>
		

		)
}

export default BlogNavbar