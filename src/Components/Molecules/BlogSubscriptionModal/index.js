import React, {useContext, useState}  from "react"
import SharpArrowBack from "react-md-icon/dist/SharpArrowBack";
import {Button} from "Components/Primitives/Button"
import {Container, Text} from "local_npm/clay-components"
import {Img} from "local_npm/react-container"
import {Link} from "react-router-dom"
import {Input as InputSubmit} from "Components/Primitives/Input/index2"
import Navbar from "./Navbar"
import {ModalContexts} from "Components/Providers/Modals"
import {subscribeKeywords} from "Components/General/registerAPI"
import {Formik} from 'formik';
const img = require("static/imgs/4. Keywords/Keywords - Suscribirte.svg");
const doneImg = require("static/imgs/4. Keywords/Keywords - Suscribirte - listo.svg");


   const verifyEmail = value => {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  };


const BlogSubscriptionModal = ()=>{

	const [succes, setSuccess] = useState(false);
	const {closeModal} = useContext(ModalContexts);




	const addMail = ({email}) => {

	
		if(!verifyEmail(email))
		{
			alert("Introduce una dirección de correo electrónico válida.")
			return
		}		

		subscribeKeywords(email).then(()=>{
			setSuccess(true)
		}).catch(()=>{
			alert("Something went wrong")
		})

	}
	


	
	return (<Container 
			width="60%" 
			margin="0 auto" 
			height="100vh" 
			background="#F8F9FA"
			border="1px solid #E3E4E7"
			tabletCSS={`
				width: 90%;
				overflow-y: scroll;
				`}
			>
			<Navbar />
			{!succes?<Container width="65%" margin="0 auto" tabletCSS={`width: 90%; margin-bottom: 200px;`}>
						<Img  
							tabletCSS={`width: 90%;`}
							width="115px" 
							src={img} 
							margin="2rem auto"/>
			
						<Text 
							onClick={addMail}
							fontSize="28px" 
							color="#202124" 
							marginB="30" 
							tabletCSS={` 
								font-size: 20px;
								line-height: 30px;


								`}
							css={`line-height:38px;`} 
							weight="500">
			
						Keywords es nuestro blog y lo creamos para compartir con la comunidad nuestros conocimientos y lo que aprendemos de la colaboración con nuestros clientes. Suscríbete y recibe nuevo contenido.
							
			
						</Text>





			
	<Formik  initialValues={{email:""}} onSubmit={addMail}>
      {props => (

        <React.Fragment>
		<InputSubmit 
		responsive
			submitClick={props.submitForm}
			onBlur={props.handleBlur}
			value={props.values.email}
			onChange={(e)=>{
				props.handleChange(e);

			}}
			name="email"
			light
			submit="Suscribirse "
			required
			type="mail"
			placeholder="Tu dirección de email"
		/>
      
       </React.Fragment>)

      }
	</Formik>
					


			
						</Container>:

						<Container width="65%" margin="0 auto"  tabletCSS={`width: 90%; margin-bottom: 3rem;`}>


								<Img  
							width="115px" 
							src={doneImg} 
							margin="2rem auto"/>



							<Container width="70%" margin="0 auto"  tabletCSS={`width: 90%; margin-bottom: 3rem;`}>
								<Text 
							fontSize="40px" 
							color="#202124" 
							marginB="30" 
							css={`line-height:50px;`} 
							weight="600">
			
					Genial, has sido suscrito con éxito.
			
						</Text>


							<Link to="/blog">
								<Button 
								onClick={closeModal}

								transparent style={{color:"#677282", marginLeft:"-0.5rem"}}> 
								<Container 
								center 
								row 
								style={{fontWeight:"500"}}>
								<SharpArrowBack style={{fontSize:"24px"}}  />

								<Text 
								fontSize="16px"
								weight="500" 
								align="left" 
								marginL="10"> 
								Regresar
								</Text> 

								</Container>
								</Button>
							</Link>



						</Container>
			
						
						
						</Container>


					}
			

		</Container>
	

		)
}

export default BlogSubscriptionModal











