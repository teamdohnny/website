import React  from 'react';
import {Container, Text} from 'local_npm/clay-components';
import uuid from "uuid/v4"
import {useShooterGetList} from "Components/Hooks/useShooterAPI";
import _ from "lodash"

const ShooterGenericList = ({Component, LoadingComponent, EmptyComponent, limit,  query,  onLoad}) => {


	const {items, loadingItems, itemsError} = useShooterGetList (query);


	let tmpItems = limit?_.take(items, limit): items;

	if(loadingItems)
		return (
			<Container height="300px" center>
				<Text fontSize="26px">
					Loading...
				</Text>
			</Container>
			)
	else if(items.length === 0)
		return (
			<Container height="300px" center>
				<Text fontSize="22px" weight="600" opacity="0.3">
					Empty
				</Text>
			</Container>
			)



	return (
		<React.Fragment>
			{tmpItems.map((item, mapIndex)=><Component {...item.data} item={item} mapIndex={mapIndex} key={uuid()}/>)}
		</React.Fragment>
);}

export default ShooterGenericList;