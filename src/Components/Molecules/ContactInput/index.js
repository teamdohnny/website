import React, {useContext, useState}  from "react"
import {Input as InputSubmit} from "Components/Primitives/Input/index2"
import {ModalContexts} from "Components/Providers/Modals"
import {newContact} from "Components/General/registerAPI"
import {Formik} from 'formik';
import UserForm from 'Components/Views/UserDataForm';


   const verifyEmail = value => {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  };

const ContactInput = (props)=>{




	const {closeModal, openModal} = useContext(ModalContexts);
	 

	const addMail = ({email}) => {		

		if(!verifyEmail(email))
		{
			alert("Introduce una dirección de correo electrónico válida.")
			return
		}


		newContact(email).then(({data})=>{

			openMoreContactInfo(data.id,email);

		}).catch(()=>{
			alert("Something went wrong")
		})

	}

	const openMoreContactInfo = (id, email) => {		

		openModal(<UserForm id={id} email={email} />,true, {background:"transparent"})

	}
	return (
	
	<Formik  initialValues={{email:""}} onSubmit={addMail}>


	
      {formikProps => (

        <React.Fragment>
		<InputSubmit 
			submitClick={formikProps.submitForm}
			onBlur={formikProps.handleBlur}
			value={formikProps.values.email}
			onChange={(e)=>{formikProps.handleChange(e);}}
			name="email"
			submit="Hablemos"
			required
			type="email"
			{...props}
		/>
  
       </React.Fragment>)

      }
	</Formik>)
}

export default ContactInput











