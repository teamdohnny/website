import React  from 'react';
import {Container, Text} from 'local_npm/clay-components';
import {Button} from "Components/Primitives/Button"
import SharpArrowBack from "react-md-icon/dist/SharpArrowBack";
import {Link} from "react-router-dom"


const ReturnButton = ({to, style = {}}) => {
  return(
    <React.Fragment>
      <Link to={to}>
        <Button transparent style={{...{position: "absolute", left: "4.5%", top: "130px",  color:"#677282"},...style}}> 
        <Container 
        center 
        row 
        style={{fontWeight:"500"}}>
        <SharpArrowBack style={{fontSize:"24px"}}  />

        <Text 
        fontSize="16px"
        weight="500" 
        align="left" 
        marginL="15"> 
          Regresar
        </Text> 

        </Container>
        </Button>
      </Link>

    </React.Fragment>
  );
}


export default ReturnButton;