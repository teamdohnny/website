import React, { Component } from 'react';
import styled from 'styled-components';
import ReactDOM from 'react-dom';
import Sidebar from "./Sidebar"
import { 
  Redirect, 
  Link,
  BrowserRouter, 
  Route, 
  Switch, 
  withRouter} from 'react-router-dom';
import HelpIcon from "react-md-icon/dist/SharpHelpOutline";
import { Button } from 'Files/Settings/Styles/Components/Button';
import RoundMenu from "react-md-icon/dist/SharpMenu";
import RoundClose from "react-md-icon/dist/SharpClose";
import {ModalsContext} from "App";

import InputSubmit from "Components/Molecules/ContactInput"

const _ = require('lodash');
const IconMenu = require("static/imgs/Resources/menu_open-24px.svg")

const UserCirlce = styled.div`
  height: 34px;
  width: 34px;
  border-radius: 50%;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  background-image: ${props => props.src? "url(" +props.src+ ")": "url('http://placehold.it/50x50')"};
`;

 const LogoTitle = styled.div`
    font-size: 25px;
`;

 const Logo = styled.img`
    padding-top:2px;
    width: 38px; 

`;

 const Nav = styled.nav`
    top:0;
    left:5%;
    position: fixed;
    overflow: hidden;
    width: 90%;

    background: white;
    ${props=>!props.noShadow && "box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);"};
    ${props=>props.border && "border-bottom:solid 1px rgba(0,0,0,0.1);"};

    color: ${props=>props.theme.color.navbarText};
    height: 71px;
    ${props=>props.theme.utils.rowContent}
    z-index:9999999999999999999999999999999;

  
`;

 const NavContent = styled.nav`
    ${props=>props.theme.utils.rowContent}
    width: 95%;
    margin: 0 auto;
   //background: red;

     @media (max-width: 950px) {
    width: 85%;
  }
`;

 const _Section = styled.div`  
    display: inline-flex;
    align-items: flex-start;
    justify-content: center;
    min-width: 0;
    height: 100%;
    flex: 1;
    z-index: 1;
     white-space: nowrap;
`;

 const AlignStart = styled(_Section)`
    justify-content: flex-start;
    order: -1;
        
`;

 const AlignEnd = styled(_Section)`
    justify-content: flex-end;
    order: 1;
    
`;

 const Separator = styled.div`
  color: #757575;
  position: relative;
  border-left: 1px solid #757575;
  width: 2px;
  height: 1.2rem;
  opacity: 0.5;
`;

const Icon = styled.i`
  vertical-align: bottom;
`;


const NavsContainer = styled.div`
  z-index:2;
${props=>props.sidebar && "z-index:99999999999999999999999999;"}
  ${props=> props.shadow && ` 
    box-shadow: 0 0.5px 3px rgba(0,0,0,0.2);
    position: fixed;
    top:0;
    width: 100%;
    height: ${65*2}px;
  `}
`;

const TabSet = styled.div`
  border: 1px red solid;
  display: ${props=>props.notNavbar?"none": "inline"};
  @media (max-width: 950px) {
    display: none;
  }
`;

 const Item = styled.div`
    position: relative; 
    height: 71px;
    cursor: pointer;
    width:auto;
    font-size: 16px;
    font-weight: 500;
    background: ${props=> props.buttonStyle?"#0035C9":"transparent" }; 


    ${props=> props.buttonStyle && "color:white;"};
    ${props=>props.theme.utils.centerContent()}
    ${props=>props.cursor && "cursor: pointer;"}
    ${props=>props.margin && "margin: 0 1.2rem;"}
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 950px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}

    ${props=>props.hide && `
        @media (max-width: 950px)
        {
          display: none;
        }
    `} 

    a{
      text-decoration: none;
      color:#677282;
      font-weight: ${props=> props.active?"500":"400" };
     ${props=> props.active && " color: #202124;"}

    }
`;

const TabItem = ({children, active,hide ,onClick})=>{

  const TabSelector = styled.div`
    background: ${(props)=>props.theme.colors.green};
    position: absolute;
    width: 65%;
    height: 4px;
    font-size: 15px;
    border-radius: 5px 5px 0 0;
    bottom: 0;
    left:17.5%;
  `;

  return(
    <Item 
    hide
      style={{margin:"0 0.8rem"}}
      onClick={onClick} 
      active={active}
      cursorPointer>
      {children}
      {active &&  <TabSelector/>}
    </Item>
    )
}


 const ButtonMore = styled.button`
  background: white;
  color: ${props=>props.theme.colors.green};
  border : none;
  box-shadow: 0 0.5px 5px rgba(0,0,0,0.2);
  width: 120px;
  position: relative;
  padding: 0.6rem 0.8rem;
  margin: 0 0.5rem;
  border-radius: 20px;
  overflow: hidden;
  cursor: pointer;
  font-size: 16px;
`;

const logo = require("static/imgs/Logos-MKT/Logo da.png")

class Header extends Component
{
  constructor(props) 
  {
      super(props)
      this.state = {
        sidebar: false,
        tutorial: false,
      }
  }
  
  render() {
    const tab = this.props.location.pathname


    var rootPath = "/";

    /*if(  this.props.story)
      rootPath = "/story"

      if(  this.props.blog)
      rootPath = "/blog"*/





    return ( 
    <ModalsContext.Consumer>
    {value=> 
    <React.Fragment>  

    <NavsContainer 
      sidebar={this.state.sidebar}
      shadow= {this.state.sidebar}>
          <Nav 
            noShadow={this.state.sidebar || tab === ("/products") || this.props.products}
            border ={tab === ("/products") || this.props.products}>
          <NavContent>
          <AlignStart>
            <Item 
              style={{
                marginRight:"30px"
              }}>
              <Link to={rootPath}>
                <Logo src={logo} />
              </Link>
            </Item>

              {/*(tab === ("/story")  || this.props.story) && 

                <Item >
                 <Link style={{marginLeft:"1rem", color:"#677282", fontSize:"16px", fontWeight:"400"}} to="/story">
                 Historias de éxito
                 </Link>
                </Item>*/}

                {/*(tab === ("/blog")  || this.props.blog) && 
                <Item  >
                <Link style={{marginLeft:"1rem", color:"#202124", fontSize:"16px", fontWeight:"400"}} to="/blog">
                 Blog
                 </Link>
                </Item>*/}
          <Item
              hide
              active = {tab === ("/services") || this.props.products}
              style={{ 
                padding:"0 0.8rem",
                fontSize:"14px",
                color:"#202124" 
              }}>

            <Link to="/services">
            Servicios
            </Link>

            </Item>

            <Item 
               
              hide
              
              active = {tab === ("/about")}
              style={{
                   padding:"0 0.8rem",
             
              fontSize:"14px",
              color:"#202124" 
              }}>

            <Link to="/about">
            Nosotros
            </Link>
            </Item>
            <Item 
               
              hide
              
              active = {tab === ("/stories") || this.props.story}
              style={{
              
                   padding:"0 0.8rem",
                fontSize:"14px",
                color:"#202124" 
              }}>

            <Link to="/stories">
            Casos de éxito
            </Link>

            </Item>

              <Item
              hide
              active = {tab === ("/blog" ) || this.props.blog}
              style={{ 
                   padding:"0 0.8rem",
                fontSize:"14px",
                color:"#202124" 
              }}>

            <Link to="/blog">
           Keywords
            </Link>
            </Item>




            
            </AlignStart>

            <AlignEnd>

              <Item hideBig
             onClick={()=>{this.setState({sidebar:!this.state.sidebar})}}>

            {this.state.sidebar?
              <RoundClose 
                style={{
                verticalAlign:"bottom",fontSize: "24px",
                marginLeft:"1rem"
                }} />:<img src={IconMenu} style={{
                  color:"blue",
                verticalAlign:"bottom",fontSize: "24px",
                marginLeft:"1rem"
                }} />
            }

            </Item>
            
            <Item hide style={{width:"400px"}}>
              <InputSubmit 
                light  
                submit="Hablemos"
                placeholder="Tu dirección de email"/>
              </Item>
            </AlignEnd>
            </NavContent>
            </Nav> 



       

</NavsContainer>



            {this.state.sidebar && 

              <Sidebar {...this.props} />
            } 

              </React.Fragment>}

          </ModalsContext.Consumer>
    
      );
  }
}

export default withRouter(Header)



//width: "100%";