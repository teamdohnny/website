import React, {useState}  from "react"
import uuid from "uuid/v4"
import {Link, withRouter} from 'react-router-dom';
import RoundClose from "react-md-icon/dist/SharpClose";
import InputSubmit from "Components/Molecules/ContactInput"
import Navbar, {Item} from "Components/Primitives/RowBox"; 
import {Container, Text} from "local_npm/clay-components"
import {Img} from "local_npm/react-container"
import {ModalContexts} from "Components/Providers/Modals"
import BlogSubscriptionModal from "Components/Molecules/BlogSubscriptionModal"
import Sidebar from "./Sidebar"

const logo = require("static/imgs/Logos-MKT/Logo da.png");
const IconMenu = require("static/imgs/Resources/menu_open-24px.svg");

const Navtext = ({children, active})=>{

  return <Text 
  fontSize="14px"
  css={` 

     text-decoration: none;
      color:#677282;
      font-weight: ${active?"500":"400" };
     ${active && " color: #202124;"}
    `}>{children}</Text>


}

const BasicNavbar = (props)=>{


  const {noshadow} = props;

  const [sidebar,setSidebar] = useState(false);

  const tab = props.location.pathname



  return (


    <React.Fragment>

    <Navbar 
      as="nav"
      defaultShadow={!noshadow}
 
      margin="0 auto"
      height="65px"
      width="90%"
      wrapper={{position:"fixed", css:"z-index: 9999999999999;"}}
      tabletCSS={`padding: 0 1rem;`}
      css={`
        padding: 0 3rem;
        max-width: 1236px;
        a{
          text-decoration: none;
        }

        ${noshadow && ` 

          border-bottom: 1px solid rgba(0,0,0,0.1);


          `}
      `}
 
    

      start={[

        <Item key={uuid()} css={`padding: 0; padding-right: 1rem;`}>
          <Link to={"/"}>
            <Img src={logo} width="38px"/>
          </Link> 
        </Item>,

        <Item
          tabletCSS={`display:none;`}
          key={uuid()}>
          

            <Link to="/services">
              <Navtext active = {tab === ("/services") || props.services}>
                Servicios
              </Navtext>
            </Link>

            </Item>,

            <Item 
               tabletCSS={`display:none;`}
              key={uuid()}
              
             >

            <Link to="/about">
              <Navtext active = {tab === ("/about") || props.about}>
                Nosotros
              </Navtext>
            </Link>
            </Item>,

            <Item 
              tabletCSS={`display:none;`}
              key={uuid()} >

            <Link to="/stories">
             <Navtext active = {tab === ("/stories") || props.stories}>
            Casos de éxito
            </Navtext>
            </Link>

            </Item>,

              <Item
              tabletCSS={`display:none;`}
              key={uuid()}
             >

            <Link to="/blog">
             <Navtext active = {tab === ("/blog") || props.blog}>
           Keywords
             </Navtext>
            </Link>
            </Item>


      ]}



    

      end={[
            <Item key={uuid()} 
            css={`display: none;`}
              tabletCSS={`display:flex;`}
             onClick={()=>{setSidebar(!sidebar)}}>

            {sidebar?<RoundClose 
                style={{color:"#677282", fontSize:"30px"}} />
                : <img
                  src={IconMenu} 
                   />
            }

            </Item>,
            
            <Item  key={uuid()} tabletCSS={`display:none; padding:0;`} style={{width:"400px"}}>
              <InputSubmit 

                light  
                submit="Hablemos"
                placeholder="Tu dirección de email"/>
              </Item>


        ]}
    />


    {sidebar && 

              <Sidebar {...props} />
            } 

    </React.Fragment>
  


   
    )
}


export {BasicNavbar};
export default withRouter(BasicNavbar);


/*



*/