import React, { Component } from 'react';
import styled from 'styled-components';
import {  Link, withRouter} from 'react-router-dom';
import {Container, Text} from 'local_npm/clay-components';
import InputSubmit from "Components/Molecules/ContactInput"

const _ = require('lodash');

 const Nav = styled.nav`
  position: fixed;
  overflow: hidden;
  top:65px;
  left:5%;
  bottom:0;
  right:5%;
  background: white;
  text-aling: left;
  z-index:99999999999999999999;
`;



 
 const Item = styled.div`
    position: relative; 
    text-aling: left;
    width: 100%;
    padding:0 5%
    margin-top: 0.5rem;
    cursor: pointer;
    color:${props=> props.active?"black":"#5F6469" };
    font-size: 20px;

    ${props=>props.cursor && "cursor: pointer;"}
  
    ${props=>props.hideBig && `
        display: none;
        @media (max-width: 950px)
        {
            ${props.theme.utils.centerContent()}
        }
    `}
    ${props=>props.hide && `
        @media (max-width: 950px)
        {
          display: none;
        }
    `}   
    a{
        text-decoration: none;
         color:black;
 
    }
`;




const Navtext = ({children, active})=>{

  return <Text 
  fontSize="30px"
  css={` 

     text-decoration: none;
      color:#677282;
      font-weight: ${active?"500":"400" };
     ${active && " color: #202124;"}
    `}>{children}</Text>


}




class Header extends Component
{
 
  
  render() {


    const tab = this.props.location.pathname;



    return (  
    <React.Fragment>  
          <Nav>
          <Container
          css={`
              width: 100%;
              margin: 2rem auto;
              a{text-decoration:none;}
            `}>


             <Item>
            <Link 

              
                    to="/services">
                    <Navtext  active={tab === ("/services" ) || this.props.services}>
                    Servicios
                    </Navtext>

                  </Link>

                 
              </Item>
                  <br/>




             <Item>
            <Link 

              
                    to="/about">
                    <Navtext  active={tab === ("/about" ) || this.props.about}>
                    Nosotros
                    </Navtext>

                  </Link>

                 
              </Item>
                  <br/>


          <Item>
            <Link 

              
                    to="/stories">
                    <Navtext  active={tab === ("/stories" ) || this.props.stories}>
                    Casos de éxito
                    </Navtext>

                  </Link>

                 
              </Item>
                  <br/>


                  <Item>
                
                  <Link 
                    style={{
                    textDecoration:"none",
                    color:"black"
                    }}
                    to="/blog">

                      <Navtext  active={tab === ("/blog" ) || this.props.blog}>
                    Keywords
                    </Navtext>

                  </Link>

                  
              </Item>
                  <br/>




           
       
           <br/>

<Container 

background="white"


css={`
    padding: 1rem;
  position: fixed;
  bottom:3rem;
  width: 80%;
  left:10%;
  right: 10%;

  `}>
           <InputSubmit 
                responsive
                light  
                submit="Hablemos"
                placeholder="Tu dirección de email"/>
             </Container>

            

            </Container>
            </Nav>  
          </React.Fragment>
      );
  }
}

export default withRouter(Header)

