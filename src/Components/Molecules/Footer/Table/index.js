import React, {Component, Fragment} from 'react';


import {
Dot,
Row,
NavItem, 
Ul, 
Container, 
Content, 
Title, 
Text, 
QuestionContainer, 
Question, 
Top, QuestionTitle, Answer } from './elements';

import {Container as ClayContainer} from 'local_npm/clay-components';

import BaselineKeyboardArrowUp from "react-md-icon/dist/SharpKeyboardArrowUp";
import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";
import {Link} from "react-router-dom"

import DATA from "../data"

class FAQ extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAnswer: false,
    }
  }

  toggleAnswer() {
    let toggle = !this.state.showAnswer;
    this.setState({showAnswer: toggle});
  }

  render() {
    return(
      <Question onClick={this.toggleAnswer.bind(this)}>
        <Top>
          <QuestionTitle>{this.props.question}</QuestionTitle>
          {this.state.showAnswer? <BaselineKeyboardArrowUp style={{
            fontSize: "24px",
            color:"white"
          }}/> : <BaselineKeyboardArrowDown style={{
            fontSize: "24px",
            color:"white"
          }}/>}
        </Top>
          {
            this.state.showAnswer?
            <Answer>
         


            {DATA.map((item, id)=> {

            if (this.props.answer === id)
    return (
        <Ul key={id}>


        {item.elements.map((li, id)=>{
          return(

            <li key={id}> 
            {li.external?<a href={li.link} target="_blank">{li.title}</a>:<Link to={li.link}>
            {li.title}
            </Link>}
          </li>
            )
        })}
        </Ul>

  );

  })} 


                  
            </Answer>:

            <Fragment>
            

      </Fragment>
    }
          
      </Question>
    )
  }
}

const FAQstructure = () => (
  <Container>
    <Content>
    <ClayContainer height="100%" center>
      <QuestionContainer>

        {DATA.map((item, id)=> <FAQ key={id} question={item.title} answer={id}/>)}

      </QuestionContainer>
   </ClayContainer>
    </Content>

  </Container>
)

export default FAQstructure;
