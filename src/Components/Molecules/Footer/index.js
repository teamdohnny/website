import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import DATA from "./data"
//import { animateScroll as scroll } from 'react-scroll'


export const Title = styled.div`
  font-size: 20px;
  font-weight: 500;
  color: white;
  margin: 0 auto;
  text-align: left;

`;


export const Content = styled.div`
	width: 90%;

	padding: 1rem 0;
	margin: 0 auto;

	${props=>props.theme.media.tablet`
	  	display: none;
		.absoluteItems
		{
			display: none;
		}
    `}
`;



const logo = require("static/imgs/Logos-MKT/Logo da.png")


const Footer = ({color, title, subtitle, img, features=[]}) => (

  <React.Fragment>
	<Container 
		css={`
		width: 100%;
		margin: 0 auto;
		text-align: left;
		background: #FFFFFF;
		position: relative;
		padding: 50px 0;
		`}>
	<Container 
		margin="0 auto"  
		width="85%"
		tabletCSS="90%"

		>
	<Container row  css={`justify-content: start;`}>



		<Container 
			width="30%" 
			center 
			tabletCSS="width:100%; ">

		<Container css="text-align: left;" >

			<Title>
				<Img src={logo} width="34px"/>
			</Title>

			<Typography
				marginT="20" 
				color="#202124" 
				fontSize="14px"   
				css="opacity:0.8; a{color: black;}">

			Hablemos - <label style={{textDecoration: "underline"}}> team@dohnny.com</label>


			</Typography>
		</Container>

		</Container>



<Container 
	width="30%"
	tabletCSS={`
		width: 100%;
	`}>
	<Container row>


	{DATA.map((item, id)=> {


			return (
			<Container 
				as="ul"
				key={id} 
				width={`${item.elements.length -1}%`}
				css={`
					color: black;
					font-size: 15px;
					display: inline-block;
					margin: 0 auto;
					text-align: left;
					white-space: nowrap;
					li{ 
						margin: 1.5rem 0;
						list-style: none;
						color:black;
						margin-left: -2.4rem;
					}
					a{

					text-decoration: none;
					color:balck;


					}
					`}

				tabletCSS={`

					width: 48%;

					`}


			>


			{item.elements.map((li, id)=>{
			return(

			<li key={id}>

			<Typography 
				weight="600" 
				css={`
				 a{
				 	font-weight: 500;
				 	color: black;
				 	text-decoration: none;
				 }
				`}>

			{li.external? <a href={li.link} target="_blank"> {li.title} </a>:<Link to={li.link}> {li.title} </Link> }
			</Typography>

			</li>
			)
			})}
			</Container>

			);

	})}	


	
	</Container>
	</Container>

	
	</Container>
	</Container>





	</Container>	
</React.Fragment>
  
);


export default Footer;


