import React, {useContext, useState} from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import axios from "axios"

export const Input = styled.input`
  width: 100%;
  padding:1.2rem 0.5rem;
  height: 50px; 
  border: none;
  font-size: 16px;
  background: white;
  margin:0;

`;

export const Button1 = styled.div`
  width: 50px;
  height: 50px; 
  background: #0035C9;
  ${props=>props.theme.utils.centerContent()} 


`;

//addNewsletter
const AddMail = () => {
   const [form, changeFormClean] = useState({ 
    name:"",
    mail:""
   });


     const [sending, setSending] = useState(false);



       const verifyEmail = value => {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  };

   const verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };

  const sendEmail = async ()=>{
        setSending(true)
    if(form.name  && form.mail && verifyEmail(form.mail) && verifyLength(form.name, 5) )
    {
      await axios.post("https://intense-shore-87101.herokuapp.com/addNewsletter",
        {...form, subject:"Registrar cliente en Newsletter -  SIAC Website"}).then(()=>{
          alert("Te has subscrito a nuestro boletín.");
        }).catch((e)=>{
          alert("Error");
          console.log(e);
        })
      }
      else 
       { 
        alert("Debes llenar todos los campos.");
        

      }


       setSending(false)
   }


   const changeForm = (data)=>{
    changeFormClean({...form, ...data })
   }

  return (
   <React.Fragment>

  
    <Container 
   
          height="100%" 
          width="85%" 
          marginCenter
          tabletCSS={`
            width:85%;
            margin: 0 auto; `}>

  
    <Container 
          row  
          center>

      <Container 
        width="44%" 
        height="100%"
        
        tabletCSS={`
            ${"width:100%;"}
          `}>

        <Typography 
          fontSize="25px"
          weight="400"
        
          css={`width:80%; line-height:35px;`}
          tabletCSS={"width:100%; margin-bottom: 4rem;"} 
          color="white">        
     Suscríbete a nuestro boletín y recibe las últimas noticias e información.

        </Typography>
      </Container>
    
    <Container width="24%"  
    css="position:relative;"


    tabletCSS={"width:100%; margin-bottom: 1rem;"}  >

     <Input required
    type="text"
     value={form.name} 
      onChange={(e)=>{
      changeForm({name:e.target.value })
      }}


    placeholder="Nombre"/>
{(form.name && !verifyLength(form.name, 5)) &&
 <label style={{position:"absolute", top: "-1.2rem", left:"0rem", color:"#E53935", fontSize:"12px"}}>El nombre debe tener al menos 5 caracteres. </label>}

    </Container>
       <Container 
       css="position:relative;"
       width="24%"  
        tabletCSS={"width:100%; margin-bottom: 1rem;"} ><Input 
       required
       type="email"
        value={form.mail} 
        onChange={(e)=>{
        changeForm({mail:e.target.value })
        }}

       placeholder="Correo electrónico"/>
             { (form.mail && !verifyEmail(form.mail)) &&  <label style={{position:"absolute", top: "-1.2rem", left:"0rem", color:"#E53935", fontSize:"12px"}}>Ingresar dirección de correo válida. </label>}
   

       </Container> 


    <Container 
    width="4%"
    tabletCSS={"margin-left: 86%; widht:12%;"}

    ><Button1 onClick={sendEmail}  disabled={sending} style={{cursor:sending?"not-allowed":"pointer"}}><ArrowIcon style={{color:"white", fontSize:"28px"}}/></Button1>
    </Container>
    </Container>
    </Container>

    </React.Fragment>
);}

export default AddMail;


/*




//siac.us11.list-manage.com/subscribe/post?u=beba3971fc7d0e22eef1cab82&id=8b28a9080d




*/