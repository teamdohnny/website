const styles = `
        *{
          text-align: left;
          color: #677282;
          font-size: 20px;
          line-height: 30px;

        }
        h1,h2,h3,h4,h5,h6{
          color:#212121;
          font-weight: 500;
        }


        h1{

          font-size: 35px;
          line-height: 45px;
        }

        h2{
          font-size: 30px;
          line-height: 40px;
        }

        h3{
          font-size: 25px;
          line-height: 35px;
        }

        h4{
          font-size: 20px;
          line-height: 20px;
        }

        h5{

          font-size: 16px;
          line-height: 16px;
        }

        h6{

          font-size: 14px;
          line-height: 14px;


        }

        img {
          width: 100%;
        }
      `;

export default styles