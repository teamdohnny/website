import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import SharpShare from "react-md-icon/dist/SharpShare";
import SharpArrowBack from "react-md-icon/dist/SharpArrowBack";
import {Link} from "react-router-dom"
import ReturnButton from "Components/Molecules/ReturnButton"
import Fade from 'react-reveal/Fade';
import _ from "lodash"
import markdownStyles from "./markdownStyles";

const ReactMarkdown = require('react-markdown')


const ShooterTemplateBasicV1 = (props) => (
  <React.Fragment>


    <Fade>
   



    <Container 
      width="85%" 
      margin="100px auto" 
      css={`position: relative; margin-top: 245px;`}  
      tabletCSS="width: 90%; margin-top: 150px; ">

        <Container tabletCSS={`display: none;`}>

      <ReturnButton style={{left:"0", top:"-100px"}} to={props.returnTo}/>
      </Container>



      <Container 
        width="75%" 
        margin="0 auto"
        tabletCSS={`
            width:100%;
          `} 
         >

        <Typography 
          fontSize="70px"
          weight="600"
          marginB="25"
          css={`
            line-height: 80px;
          `}
          tabletCSS={`
            font-size:40px; 
            line-height:50px;
            text-aling: left;
          `}  
          color="#000">
          {_.get(props, "data.titulo")}
        </Typography>
    
        <Typography 
          fontSize="25px"
          weight="400"
          marginB="20px"
          css="line-height: 35px;"
          tabletCSS={`
            margin-top: 40px;
            font-size:20px; 
            line-height:30px;
            width: 100%;
            `} 
          color="#202124">
         {_.get(props, "data.descripcion")}
        </Typography>

      </Container>

      <Container   
        css="margin-top: 100px; position: relative;" 
        tabletCSS="width: 100%; margin: 2rem auto;">
        <Img 
            corner="10px"
            defaultShadow
            overflowHidden
            src={_.get(props, "data.cover")}
            />
      </Container>
    </Container>
  </Fade>


  <Container    
    margin="100px auto" 
    css="width:50%; text-aling:left;" 
    tabletCSS="width:90%;">

    <Container
      css={markdownStyles}
      >
      <ReactMarkdown source={_.get(props, "data.historia.markdown")}/>
    </Container>
  </Container>

</React.Fragment>
);

export default ShooterTemplateBasicV1;