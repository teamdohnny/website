const Brands = [



{
	img: require("static/imgs/3. Nosotros/Descubrir.svg"),
	title:"Descubrir",
	subtitle:"Hablamos con usted, después investigamos... investigamos mucho."

},

{
	img: require("static/imgs/3. Nosotros/Ejecución.svg"),
	title:"Ejecución",
	subtitle:"Diseñamos el plan para poner en marcha tu proyecto."

},


{
	img: require("static/imgs/3. Nosotros/Estrategia.svg"),
	title:"Estrategia",
	subtitle:"Sesiones de UX, entrevistas, diseño, desarrollo. Lo necesario."

},
{
	img: require("static/imgs/3. Nosotros/Lanzar.svg"),
	title:"Lanzar",
	subtitle:"Es hora de anunciar lo creado al mundo. Te guiamos en el proceso."

},

{
	img: require("static/imgs/3. Nosotros/Mantener.svg"),
	title:"Mantener",
	subtitle:"Seguimos retocando y manteniendo tu producto."

},








 ];


 export default Brands
/*

Logo AGROFIRME.png
Logo Aguascalientes.png
Logo ALMER.png
Logo CONSEDE.png
Logo FIDEAPECH.png
Logo CREDIAN.png
Logo FINPO.png
Logo FONDO PLATA.png
Logo KRTC.png
Logo LINGO.png
Logo Mega.png
Logo PRETMEX.png
Logo TDM.png
Logo TIO FIO.png

*/