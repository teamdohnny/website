const Members = [{
	img: require("static/pictures/03. Porqué nosotros/Eduardo.png"),
	name:"Eduardo Arreola",
	title:"Cofundador y Director general",
	subtitle:`"SIAC nos recuerda que no debemos perder de vista el cumplimiento regulatorio, adoptando y adaptando las mejores practicas en los procesos de crédito"`

},

{
	img: require("static/pictures/03. Porqué nosotros/Agustin.png"),
	name:"Agustín Zambrano",
	title:"Cofundador y Director de tecnología",
	subtitle:`"La flexibilidad y la cercanía con el cliente distingue a SIAC como un aliado estratégico para el desarrollo de sus clientes"`

},

{
	img: require("static/pictures/03. Porqué nosotros/VictorL.png"),
	name:"Víctor Luna",
	title:"Gerente comercial",
	subtitle:`"Nuestras herramientas tecnológicas han adoptado las mejores prácticas de nuestros clientes gracias a la colaboración mutua".`

},
{
	img: require("static/pictures/03. Porqué nosotros/Alberto.png"),
	name:"Alberto Demongin",
	title:"Gerente de Servicios al cliente",
	subtitle:`“SIAC, soluciones para administrar portafolios de financiamientos o arrendamientos para que nuestros clientes puedan operar con el control requerido.”`

},

{
	img: require("static/pictures/03. Porqué nosotros/Rogelio.png"),
	name:"Rogelio Mares",
	title:"Gerente de desarrollo",
	subtitle:`"SIAC es más que una herramienta, es una definición de experiencia y buenas prácticas".`

},

{
	img: require("static/pictures/03. Porqué nosotros/Dassaev.png"),
	name:"Dassaev Urrutia",
	title:"Administración de proyectos",
	subtitle:`"La cercanía como principio para diseñar soluciones, siempre pensando en sus necesidades".`

},

{
	img: require("static/pictures/03. Porqué nosotros/Jatsi.png"),
	name:"Jatsirani De la Cruz",
	title:"Gerente de Administración",
	subtitle:`"Seleccionar y organizar los mejores recursos para obsequiar a nuestros clientes la mejor calidad en software y mejores prácticas a IFNB".`

},

{
	img: require("static/pictures/03. Porqué nosotros/VictorG.png"),
	name:"Víctor González",
	title:"Consultor",
	subtitle:`Experto en diseño de políticas públicas en materia de financiamiento para el desarrollo, competitividad y productividad, planeación estratégica y liderazgo.`

},





 ];


 export default Members
