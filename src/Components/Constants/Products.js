const Products =  {
	"siac-suite":{
		title:"SIAC",
  		subtitle:"Suite",
  		color:"#0035C9",
  		link:"/product/siac-suite",

  		summary:`Software para la administración de cartera. Permite controlar de manera eficiente las exigencias operativas desde el conocimiento del cliente (KYC), el análisis de viabilidad y originación del crédito, el arrendamiento o el factoraje financiero, su recuperación y el correcto registro en la contabilidad.
		`,

		description:`Software para la administración de cartera. Permite controlar de manera eficiente las exigencias operativas desde el conocimiento del cliente (KYC), el análisis de viabilidad y originación del crédito, el arrendamiento o el factoraje financiero, su recuperación y el correcto registro en la contabilidad.`,
		img: require("static/pictures/02. Productos/01. SIAC Suite/SIAC Suite.png"),
		illustration: require("static/pictures/02. Productos/01. SIAC Suite/Ilustración SIAC Suite.png"),


		features:[{
			icon:null,
			title:"",
			description:``,
		}],
		next:"siac-leasing",
		back:"",
		features:[
			{
				icon: require("react-md-icon/dist/SharpAssessment"),
				title: `Análisis de viabilidad`,
				subtitle: `
				Control de la originación del crédito, arrendamiento o factoraje desde la solicitud, evaluación paramétrica, mesa de control, digitalización del expediente, garantías, referencias crediticias, avales, consulta a los burós de crédito, niveles de resolución y comité, impresión de documentos de contratación y documentos relacionados con la formalización.
				`,

			},
			{
			icon: require("react-md-icon/dist/SharpCreditCard"),
			title: `Administración de cartera crediticia`,
			subtitle: `Administración de créditos simple, avios, refaccionarios, microcréditos grupales e individuales, líneas revolventes, hipotecarios, prendarios y automotriz.
`
			},
			

				{
			icon: require("react-md-icon/dist/SharpLocalLaundryService"),
			title: `Prevención de lavado de dinero	`,
			subtitle: `Software de PLD para el cumplimiento de disposiciones de prevención de lavado de dinero para entidades financieras y comerciales (Actividades Vulnerables).`
			},
			{
			icon: require("react-md-icon/dist/SharpBusiness"),
			title: `Arrendamiento y factoraje financiero`,
			subtitle: `Gestión de arrendamiento puro y financiero. Administración de factoraje financiero.` 
			},
		
			{
			icon: require("react-md-icon/dist/SharpAssignmentTurnedIn"),
			title: `Control de garantías`,
			subtitle: `Administración de garantías líquidas, hipotecarias, prendarias, fianzas y fideicomisos.`
			},
				{
				icon: require("react-md-icon/dist/SharpMoney"),
				title: `Administración de fondeos`,
				subtitle: `Administración de líneas de fondeo: FIRA, Financiera Rural, PRONAFIM, Banca Múltiple, Fondeadores internacionales. Vinculación de cartera activa a pasiva, estado de cuenta y reportes por entidad fondeadora.`
			},
			{
				icon: require("react-md-icon/dist/SharpTouchApp"),
				title: `Interfaz contable`,
				subtitle: `Intercambio de la información de cartera a la contabilidad.`
			},
			
			
			{
			icon: require("react-md-icon/dist/SharpAccountBalanceWallet"),
			title: `Ahorro e inversión`,
			subtitle: `Administración de cuentas de captación. Estado de cuenta, reportes para la gestión de ahorro e inversión.`
			},

			{
			icon: require("react-md-icon/dist/SharpImportExport"),
			title: `Venta y acopio`,
			subtitle: `Software especializado en Parafinancieras. Administración de créditos agropecuarios monetarios y en especie. Control de entradas y salidas a almacenes de productos.
`
			},
		

		]
	},













//LEASING




		"siac-leasing":{
		title:"SIAC",
  		subtitle:"Leasing",
  		color:"#FDC638",
  		link:"/product/siac-leasing",
		description:`Software especializado en la administración y gestión de operaciones de arrendamiento puro y financiero, dirigido a arrendadoras que buscan optimizar la operación, seguimiento, el control de fondeo, facturación múltiple, administración de valores residuales, seguros y reportes para la bursatilización de cartera.`,
		summary:`Software especializado en la administración y gestión de operaciones de arrendamiento puro y financiero, dirigido a arrendadoras que buscan optimizar la operación, seguimiento, el control de fondeo, facturación múltiple, administración de valores residuales, seguros y reportes para la bursatilización de cartera.
		`,

		img:require("static/pictures/02. Productos/02. SIAC Leasing/SIAC Leasing.png"),
		illustration: require("static/pictures/02. Productos/02. SIAC Leasing/Ilustración SIAC Leasing.png"),

	
		next:"siac-actividades-vulnerables",
		back:"siac-suite",


		features:[
			{
				icon: require("react-md-icon/dist/SharpStore"),
				title: `Originación y conocimiento del cliente`,
				subtitle: `Manejo de solicitud, comité de aprobación y KYC cumplimiento de LPLDyFT. ` 

			},

			{
				
				icon: require("react-md-icon/dist/SharpAccessTime"),
				title: `Control de operación de arrendamiento puro y financiero`,
				subtitle: `Distintos esquemas de operación según los requerimientos y modelo de negocio.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpTimelapse"),
				title: `Rentas constantes, programadas y extraordinarias`,
				subtitle: `Definición de la condiciones del arrendamiento para el diseño de productos predeterminados o personalizaciones según requerimientos de sus clientes.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpTableChart"),
				title: `Administración de pólizas de Seguro`,
				subtitle: `Registra las pólizas, datos del seguro y la información relacionada.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpTrendingDown"),
				title: `Cotizador y simulador financiero`,
				subtitle: `Facilita la emisión de opciones y escenarios para el cliente.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpFeaturedVideo"),
				title: `Depreciación de bienes`,
				subtitle: `Control del valor de los bienes arrendados a través del tiempo.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpAccountBalance"),
				title: `Facturación y Cobranza`,
				subtitle: `Generación de facturas simples, múltiples y reportes de recuperación periódicos.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpPlaylistAddCheck"),
				title: `Líneas revolventes para flotillas`,
				subtitle: `Gestión de flotillas que facilitan su administración global y el registro de incidentes individuales.` 
			},

			{
				
				icon: require("react-md-icon/dist/SharpFilter9Plus"),
				title: `Administración del valor residual y fondeo`,
				subtitle: `Opciones de liquidación, renovación o financiamiento del valor residual así como el control de los saldos pasivos de su organización.` 
			},
			{
				
				icon: require("react-md-icon/dist/SharpVerticalSplit"),
				title: `Interfaz con contabilidad`,
				subtitle: `Integración con su sistema contable acorde a las políticas y criterios de su organización.` 
			},
			{
				
				icon: require("react-md-icon/dist/SharpDeviceHub"),
				title: `Conexión con terceros`,
				subtitle: `Consultas a las SICs y proveedores de listas negras.` 
			},

				{
				
				icon: require("react-md-icon/dist/SharpReport"),
				title: `Reportes para bursatilización`,
				subtitle: `Información para el aforo y colocación de ofertas públicas y privadas.` 
			},



		]
	},





	// Actividades vulnerables




		"siac-actividades-vulnerables":{
		title:"SIAC",
  		subtitle:"Actividades vulnerables",
  		color:"#9300C9",
  		link:"/product/siac-actividades-vulnerables",
		description:`Software para la gestión de actividades vulnerables definidas en la Ley Federal para la Prevención e Identificación de Operaciones con Recursos de Procedencia Ilícita. Administración de información de las operaciones, control de expediente de clientes o usuarios y generación de avisos sobre actividades y operaciones vulnerables de las personas físicas o morales que están obligados a presentarlos.`,
		summary:`

		Software para la gestión de actividades vulnerables definidas en la Ley Federal para la Prevención e Identificación de Operaciones con Recursos de Procedencia Ilícita. Administración de información de las operaciones.

		`,
		illustration: require("static/pictures/02. Productos/03. SIAC Actividades Vulnerables/Ilustración SIAC AV.png"),
		img:require("static/pictures/02. Productos/03. SIAC Actividades Vulnerables/SIAC AV.png"),
		features:[
			{
				
				icon: require("react-md-icon/dist/SharpAccessibility"),
				title: `Servicio de mutuo, préstamo o crédito`,
				subtitle: `El ofrecimiento habitual y/o profesional de operaciones de mutuo, garantía u otorgamiento de préstamos o créditos, con o sin garantía, por parte de sujetos distintos a las Entidades Financieras.`
			},
					{
				
				icon: require("react-md-icon/dist/SharpHome"),
				title: `Compra y venta de inmuebles`,
				subtitle: `La prestación habitual y/o profesional de servicios de construcción, desarrollo de bienes inmuebles, de intermediación en la transmisión de la propiedad o constitución de derechos sobre dichos bienes, en los que se involucren operaciones de compra-venta de los propios bienes, por cuenta o a favor de clientes de quienes presten dichos servicios.`
			},
					{
				
				icon: require("react-md-icon/dist/SharpDirectionsCar"),
				title: `Comercialización de vehículos`,
				subtitle: `La comercialización y distribución habitual y/o profesional de vehículos, nuevos o usados, ya sean aéreos, marítimos o terrestres.`
			},
					{
				
				icon: require("react-md-icon/dist/SharpThumbUp"),
				title: `Donativos`,
				subtitle: `La recepción de donativos, por parte de las asociaciones y sociedades sin fines de lucro.`
			},
		],
		next:"siac-consulting",
		back:"siac-leasing"
	},










	// Consulting






		"siac-consulting":{
		title:"SIAC",
  		subtitle:"Consulting",
  		color:"#1CAD6C",
  		link:"/product/siac-consulting",
		description:`Servicios de consultoría dirigidos al fortalecimiento y sustentabilidad de las instituciones financieras no bancarias promoviendo la adopción de mejores prácticas y lineamientos regulatorios recomendados o exigidos por la autoridad supervisora.`,

		summary:`Servicios de consultoría dirigidos al fortalecimiento y sustentabilidad de las instituciones financieras no bancarias promoviendo la adopción de mejores prácticas y lineamientos regulatorios recomendados o exigidos por la autoridad supervisora.

		`,

		img:require("static/pictures/02. Productos/04. SIAC Consulting/SIAC Consulting.png"),
		illustration: require("static/pictures/02. Productos/04. SIAC Consulting/Ilustración SIAC Consulting.png"),
		
		features:[
			{

				icon: require("react-md-icon/dist/SharpSchool"),
				title: `Capacitación`,
				subtitle: `Contamos con diversos cursos de capacitación, teórico-prácticos que coadyuvan en el fortalecimiento de las áreas de promoción, administración de cartera, cobranza y contabilidad financiera.`

			},

			{

				icon: require("react-md-icon/dist/SharpLocalLibrary"),
				title: `Consultoría`,
				subtitle: `Diligencias para inversión, calificación (rating) financiero y social, desarrollo de esquemas de autorregulación, definición de procesos, elaboración de manuales y planes de negocios para empresas financieras no bancarias privadas y públicas.`
			}
		],
		next:"",
		back:"siac-actividades-vulnerables"
	},
}


export default Products;