import axios from "axios";

/*
    This file will define the way to 
*/

//[actions]

export const subscribeKeywords = (email)=>{


	return new Promise((resolve, rejected)=>{

			 axios.post(`${process.env.REACT_APP_SHOOTER_API}/form/create`,{email, formId:"keywords_subscrition"}).then((res)=>{

            resolve(res)
          
        
        }).catch((data)=>{

        	rejected(data)
        })
	})
 
}




export const newContact = (email)=>{
	return new Promise((resolve, rejected)=>{

			 axios.post(`${process.env.REACT_APP_SHOOTER_API}/form/create`,{email, formId:"da_prospect"}).then((res)=>{

            resolve(res)
          
        
        }).catch((data)=>{

        	rejected(data)
        })
	})
 
}


export const updateContactData = (id, data)=>{


	return new Promise((resolve, rejected)=>{

			 axios.post(`${process.env.REACT_APP_SHOOTER_API}/form/update`,{...data, id, formId:"da_prospect"}).then((res)=>{

            resolve(res)
          
        
        }).catch((data)=>{

        	rejected(data)
        })
	})
 
}



