import React, {useState, useContext} from 'react';
import Navbar from "Components/Molecules/Navbar"
import Footer from "Components/Molecules/Footer"
import ContactForm from "Components/Molecules/ContactForm"
import Header from "./Header"
import Services from "./Services"
import {Container} from 'local_npm/clay-components';
import {bodyStyle} from "clay-styles"



const ServicesView = () => {
  return(
    <React.Fragment>
    
      <Navbar />

      <Container {...bodyStyle}>
        <Header/>
      
        <Services/>
        
        <br/>
        <br/>
        <br/>

        <ContactForm />
        <Footer />
      </Container>
    </React.Fragment>
  );
}


export default ServicesView;