import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/clay-components';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {  Link} from 'react-router-dom';
 import SharpPlayArrow from "react-md-icon/dist/SharpPlayArrow";

import Brands from "Components/Molecules/Brands"
 import Fade from 'react-reveal/Fade';

import ServiceInfoCard from "Components/Molecules/ServiceInfoCard"

import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";

const Services = () => (
   <React.Fragment>



<Container  margin="100px auto" corner="10px" tabletCSS={`margin-top: 50px;`} >

<ServiceInfoCard  />
<ServiceInfoCard 
	img={require("static/imgs/2. Servicios/Diseño.png")}
	title={"Diseño."}
	features={[{
	            title:"Branding", 
	            subtitle:`Construimos marcas con personalidad especialmente diseñada para atraer y enamorar a tus usuarios/clientes. Miramos todos los puntos necesarios: Competencias, estrategia de comunicación, implementación, etc.`
	            },
	            {
	            title:"Diseño UI/UX", 
	            subtitle:`Creamos experiencias digitales consistentes que basados en las necesidades de tus usuarios/clientes den como resultado interfaces de usuario atractivas y fáciles de usar.`
	            },
	            {
	            title:"Prototipado", 
	            subtitle:`Diseñamos prototipos de productos digitales tan clicables que parecerán la versión final. Úsalo para testear con tus usuarios, descubre cosas interesantes y mejóralo.`
	}]} />

<ServiceInfoCard
	img={require("static/imgs/2. Servicios/Desarrollo.png")}
	title={"Desarrollo."}
	features={[{
	            title:"Sitios web", 
	            subtitle:`Diseñamos y desarrollamos sitios web que cuenten una historia apegada a tu marca enfocado en cumplir tus objetivos. Recabar datos o vende un producto/servicio.`
	            },
	            {
	            title:"Aplicaciones web", 
	            subtitle:`Diseñamos y desarrollamos aplicaciones web simples y complejas con enfoque en UX y escalabilidad. Construimos software que no apesta y que es fácil de mantener.`
	            },
	            {
	            title:"Aplicaciones progresivas", 
	            subtitle:`Diseñamos y desarrollamos aplicaciones web progresivas que funcionan como nativas de Android y iOs, pero sin toda la complejidad de desarrollar para cada dispositivo. Enfoque en mantenimiento y escalabilidad.`
	}]}


 />


</Container>



    </React.Fragment>
);

export default Services;