import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Container from 'Files/Settings/Styles/Components/Container';
import { Content, Info, Title, ImageContainer, Image } from './elements';
import { Button } from 'Files/Settings/Styles/Components/Button';
 

class FourOhFour extends Component {
	render() {
		return(
			<Container>
				<Content>
					<Info>
						<ImageContainer style={{width:"100px", margin:"0 auto"}}>
							<Image src={require("static/imgs/Logos-MKT/Logo da.png")}/>
						</ImageContainer>
						
						
						<Title><h4>ERROR 404</h4> Página no encontrada. </Title>
						
						<Link to="/">
							<Button style={{height:"50px", fontSize:"18px", background:"#212121"}}>Ir al inicio</Button>
						</Link>
					</Info>
				</Content>
			</Container>
		)
	}
}

export default FourOhFour;

