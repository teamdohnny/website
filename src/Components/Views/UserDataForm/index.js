import React, {useState, useContext} from 'react';
import {Container,Text} from 'local_npm/clay-components';
import {Img} from 'local_npm/react-container';
import {Input} from "Components/Primitives/Input";
import {Link} from "react-router-dom";
import {Formik} from 'formik';
import {updateContactData} from "Components/General/registerAPI";
import {ModalContexts} from "Components/Providers/Modals"
import {bodyStyle} from "clay-styles"



const img = require("static/imgs/5. Contactanos/Se cliente.svg");
const succesImg = require("static/imgs/5. Contactanos/Formulario enviado.svg");
const logo = require("static/imgs/Logos-MKT/Logo da SVG.svg");

   const verifyEmail = value => {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  };

const Succes = ({closeModal})=>{

 

  return (

    <Container  center height="100vh" css=" overflow-y: scroll;">

      <Container row width="70%" height="270px"  tabletCSS={`width: 90%;  margin: 0 auto; height: auto;`} >
      <Container width="40%"  tabletCSS={`width: 100%; `}>
       <Img src={succesImg}  height="100%" center tabletCSS={`margin: 40px auto; width: 100%; height: auto;`}/>
      </Container>


      <Container width="50%" css={` `} tabletCSS={`width: 100%; `}>

      <Text fontSize="40px" marginB="20" weight="600"css={` line-height:50px;`}>
      Tus datos fueron enviados con éxito.
      </Text>


      <Text 
      marginB="30"
      fontSize="20px"  
      weight="400" 
      color="#677282"
      css={`
      line-height:30px;
      `}>
      Estaremos revisando tu información, y pronto te contactaremos. Besos😘

      </Text>
      <Container css={`

      text-align: left;
      `}>

      <Link to={"/"}>


      <Container
      onClick={closeModal}
      css={` 
      border-color: black;
      cursor: pointer;
      `}
      width="160px"
      height="41px"
      defaultCorner
      defaultBorder
      background="transparent"
      as="button"
      >
      Volver al inicio
      </Container>
      </Link>
      </Container>
      </Container>
      </Container>
    </Container>

    )}



const Footer = (props)=>(      

  <Container {...props}>

    <Img src={img} />


    <Text fontSize="18px"  marginT="20" weight="400" color="#677282"css={`line-height:30px;`}>
      <b style={{color:"black"}}>Llámanos</b> - + 52 33 26 20 86 08
    </Text>


    <Text 
      fontSize="18px"  
      weight="400" 
      color="#677282"
      css={` line-height:30px;`}>
      <b style={{color:"black"}}> Envía un email</b> - team@dohnny.com
    </Text>
    </Container>)

  
const UserDataForm = ({id, email = ""}) => {

  
  const [succes, setSuccess] = useState(false);
  const {closeModal} = useContext(ModalContexts);


  const updateContact = (data) => {

    if(!data.name || !data.email)
    {
      alert("Debes introducir almenos tu nombre y correo para continuar.")
      return
    }

    if(!verifyEmail(data.email))
    {
      alert("Introduce una dirección de correo electrónico válida.")
      return
    }


    updateContactData(id,data).then(()=>{
      setSuccess(true)
    }).catch(()=>{
      alert("Something went wrong")
    })
  }


  return(
  <React.Fragment>

  <Container position="fixed" distance="0 0 0 0" background="#F2F4F5;" css="overflow-y: scroll;" >
     <Container {...bodyStyle}>


      {(succes)? <Succes closeModal={closeModal}/>: 

     <Container row>

     <Container width="60%"

     tabletCSS={`width: 100%; `}

      css={`

        padding: 0 2rem 4rem 2rem;

        margin-top: 126px;

      `}>
    
        <Text fontSize="70px" marginB="20" weight="600"css={` line-height:80px;`}  tabletCSS={`
            text-align: left;
            font-size:45px;
            line-height: 55px;
          `} >
        Conviértete en nuestro cliente.
       </Text>


       <Text 
        marginB="50"
        fontSize="20px"  
        weight="400" 
        color="#202124"
        css={`
          line-height:30px;
      `}

       tabletCSS={`
            text-align: left;
  
          `} 

      >
    Nos gusta involucrarnos por completo con nuestros clientes, eso quiere decir que la colaboración con nosotros puede ser larga. Los mejores resultados se obtienen trabajando juntos y por periodos largos. Ingresa tus datos en el siguiente formulario y en un par de días nuestro equipo se pondrá en contacto contigo. Sin compromisos.
     </Text>
      

<Footer tabletCSS={`display: none; `}/>



     </Container>

     <Container width="40%" background="#212121"  tabletCSS={`width: 100%; `}>
     <Container  


     tabletCSS={`position: relative; width: 100%; right: auto`}


     width="36%"
       
     position="fixed"
     distance="0 5% 0 auto"
     css={`



      `}>

    <Text fontSize="30px" color="white" align="center"  marginT="30" weight="500"css={` 


      line-height:40px;
      `}>
     Llena el formulario
       

     </Text>


     <Container width="80%" margin="2rem auto">





<Formik
      initialValues={{name:"", email}}
      onSubmit={updateContact}>
      {props => (
    <React.Fragment>
    
      <Input 
        placeholder="Tu nombre"  
        name="name"  
        onBlur={props.handleBlur}
        value={props.values.name}
        onChange={(e)=>{
                props.handleChange(e);
        }}
      />

      <Input 
        placeholder="Nombre de tu compañia"   
        name="company"
        onBlur={props.handleBlur}
        value={props.values.company}
        onChange={(e)=>{
                props.handleChange(e);
        }}
      />


      <Input 
        placeholder="Dirección Email"   
        name="email"
        onBlur={props.handleBlur}
        value={props.values.email}
        onChange={(e)=>{
                  props.handleChange(e);       
        }} />
      <Input placeholder="Numero de teléfono"   name="phone"

       onBlur={props.handleBlur}
              value={props.values.phone}
      onChange={(e)=>{
                props.handleChange(e);
                


              }}/>
      <Input style={{minHeight:"100px"}} placeholder="¿En que podemos ayudarte?" name="comment" as="textarea"

       onBlur={props.handleBlur}
              value={props.values.comment}
      onChange={(e)=>{
                props.handleChange(e);
                


              }}/>

       
      <Container css="text-align: right;">
            <Container
             onClick={props.submitForm} 
              as="button" 
              background="white" 
              height="41px" 
              width="160px" 
              css="color:black; cursor: pointer;" 
              border="none" 
              defaultCorner 
              margin="2rem 0">
            Enviar
            </Container>
      </Container>
       </React.Fragment>)

      }
      </Formik>






     </Container>

     </Container>
       
     </Container>
        <Footer css="display: none;" tabletCSS={`display: inline-block; margin: 3rem auto; width: 90%;`}/>


       </Container>}





       <Container 
          height="65px" 
          center 
          width="100px" 
          position="absolute" 
          css={`
              cursor: pointer;
              top:0;
              `}>

        <Link to={"/"}>
          <Img onClick={closeModal} src={logo} width="38px"/>
        </Link> 

</Container>
   </Container>

      </Container>
  </React.Fragment>
  )
}
export default UserDataForm;
