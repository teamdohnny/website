import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import Brands from "Components/Molecules/Brands"

const Customers = () => (
   <React.Fragment>
    <Container  
      width="90%" 
      margin="150px auto">
      
       <Typography 
          fontSize="70px"
          weight="600"
          css={` width: 100%; margin: 0 auto;`}
          align="center"
          tabletCSS={`
            font-size:45px;
            line-height: 55px;
          `} 
          color="#212121">Nuestros clientes.
          </Typography>
  
          <Brands />
    
    </Container>
    </React.Fragment>
);

export default Customers;