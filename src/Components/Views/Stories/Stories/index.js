import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img,StyledContainer} from 'local_npm/react-container';

import {Button, ArrowIcon} from "Components/Primitives/Button";


import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";


import Bounce from 'react-reveal/Bounce';
import withReveal from 'react-reveal/withReveal';
import {Link} from "react-router-dom"
import _ from "lodash"

const StoriesList = ({items}) => {
   const [showAll, setShowAll] = useState(3)

    //const Animation = withReveal (Container ,<Bounce cascade bottom delay={100} /> )
 
const Animation = (props)=> {

    /*

    if(props.delay < 2)
        return <Container {...props}/>

    */


    const TMP = withReveal (Container ,<Bounce cascade bottom delay={props.delay} /> )

    return <TMP {...props}/>

}


    return (
<React.Fragment>
   {(items.length === 0 ||!items)?<Container height="500px" center>
<Img src={require("static/pictures/iconos/No hay.png")} width="120px"/>
<br/>
<br/>
<Typography fontSize="22px">Aún no hay posts nuevos</Typography>
</Container>:  <Container
        tabletCSS="width:90%;" 
        width="85%" 
        margin="110px auto">

       <Typography 
            fontSize="60px" 
            color="black"
            weight="500"
            marginB="20"
             tabletCSS={`
        
            font-size:35px; 
            line-height:45px;
           
            `}    
            css={`line-height:70px;`}>
    Nuestras historias de éxito.
        </Typography>

        </Container>  



}

   
    <Container
        
        tabletCSS="width:90%;" 
        width="85%" 
        margin="110px auto">






   {_.map(_.take(items, showAll),(item, key)=>( 


      <Animation
           
         
            key={"12" + key} 
            border="1px solid #212121"
            width="100%"
            height= "460px"
             tabletCSS={"height: auto; width:90%;"}
            overflowHidden
            css={`padding:0rem; position: relative; margin: 2rem auto; position: relative;`}>
            <Link to={"/story/"+item.id} style={{textDecoration:"none", color:"#212121"}}>
            
            <Container row height="100%">

        <Container
            width="50%"
            height="100%"
            background="#0035C9"
            backgroundImg={`"${_.get(item,"data.cover")}"`}
            src=""
            tabletCSS={"width:100%; margin: 0 auto; height 250px;"}
            css="padding: 0rem;"
          
            overflowHidden/>


    <Container width="50%"  height="100%"  css="padding: 4rem 2rem; position:relative;"  tabletCSS={"width:100%;"}>


    <Container>

        <Typography
            align="left"
            weight="500"
            fontSize="45px"
            css={`line-height:55px;`}
            marginB="20">

            {_.get(item,"data.titulo")}

        </Typography>

        <Typography 
            fontSize="20px" 
            color="#677282"
            marginB="20"
            tabletCSS="margin-bottom: 50px;"
            css={`line-height:30px;`}>
            {_.get(item,"data.descripcion")}
  
        </Typography>




       
            </Container>

            </Container>

            </Container>










            </Link>
            </Animation>







    ))}



       
    </Container>

   <Container 
    center 
    height="100px" 
    css="margin-bottom: 100px; cursor:pointer;" 
    color="#212121"> 

{(items.length >= 3 && showAll <= items.length) &&
<Container 
    row 
    width="10%"
    onClick={()=>{setShowAll(showAll + 3)}}
    tabletCSS={"width:90%; margin: 0 auto;"}

     >


<Container 

    width="80%" 
    css="font-weight:500; font-size: 18px;">
Mostrar más
</Container>

<Container width="20%" css="font-weight:500; font-size: 24px;">
 <BaselineKeyboardArrowDown/>
</Container>




</Container>}
</Container>

{/*
    <Container 
    center 
    height="100px" 
    css="margin-bottom: 100px; cursor:pointer;" 
    color="#212121"> 


<Container 
    row 
    width="10%" 
        tabletCSS={"width:90%; margin: 0 auto;"}
    >


<Container 
    width="80%" 
    css="font-weight:500; font-size: 18px;">
Mostrar más
</Container>

<Container width="20%" css="font-weight:500; font-size: 24px;">
 <BaselineKeyboardArrowDown/>
</Container>




</Container>


    </Container>*/}





    </React.Fragment>
);}

export default StoriesList;