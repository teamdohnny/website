import React  from 'react';
import Navbar from "Components/Molecules/Navbar"
import Footer from "Components/Molecules/Footer"
import ContactForm from "Components/Molecules/ContactForm"
import StoriesList from "./StoriesList"
import Customers from "./Customers"
import styled from 'styled-components';
import {Container} from "local_npm/clay-components";
//import {Container} from 'local_npm/react-container';
//import Bottom from "Components/Molecules/Bottom";


import {bodyStyle} from "clay-styles"

export const Body = styled.div`
  max-width: 1400px;
  width: 90%;
  background: white;
  margin: 0 auto;
  overflow: hidden;
  text-align: center;
  box-shadow: 0px 0.5px 3px #00000033;
`;
 


const Stories = () => {
  return(
    <React.Fragment>
      <Navbar />
      <Container {...bodyStyle}>

        <StoriesList />
        <Container height="1px" background="rgba(0,0,0,0.2)" />
        <Customers />
        <ContactForm />
        <Footer />
      </Container>
    </React.Fragment>
  );
}


export default Stories;