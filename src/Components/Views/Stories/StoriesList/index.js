import React from 'react';
import styled from 'styled-components';
import {Container, Text} from 'local_npm/clay-components';
import Fade from 'react-reveal/Fade';
import {Link} from 'react-router-dom';
import StoryCard from "Components/Molecules/StoryCard";
import ShooterGenericList from "Components/Molecules/ShooterGenericList";

const StoriesList = () => (

   <React.Fragment>
    <Fade cascade big>
      <Container  
        width="85%"
        margin="0 auto" 
        css="margin-top:170px; "  
        tabletCSS={`
          width: 90%;
          padding-right: 10%;
          margin-top: 120px;
          `}>
        <Container 
          width="100%"
          css="text-align:center;"
         >

          <Text 
            fontSize="70px"
            weight="600"
            css={` width: 100%; margin: 0 auto;`}
            align="center"
            tabletCSS={`
              font-size:45px;
              line-height: 55px;
              text-align: left;
            `} 
            color="#212121">
         Casos de éxito.
          </Text>
        </Container>

      </Container>
    </Fade>


  <Container 
    wrapper={{width:"90%", margin:"0 auto"}}  
    css="padding:-1rem;" 
    margin="50px auto" 
    row>
      <ShooterGenericList 
          Component={StoryCard} 
          query={{params:{type:"story"}}} />

  </Container>
  </React.Fragment>

);

export default StoriesList;