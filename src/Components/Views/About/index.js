import React, {useState, useContext} from 'react';
import Navbar from "Components/Molecules/Navbar"
import Footer from "Components/Molecules/Footer"

import ContactForm from "Components/Molecules/ContactForm"
import Header from "./Header"
import Portfolio from "./Portfolio"
import Services from "./Services"
import Community from "./Community"
import styled from 'styled-components';
import {Container} from 'local_npm/react-container';

import {bodyStyle} from "clay-styles"

const Landing = () => {
  return(
    <React.Fragment>
      <Navbar />
      <Container {...bodyStyle}>
        <Header/>
        <Portfolio />
        <Services/>
        <Container css="height:1px; background: #E3E4E7;"/>
        <Community />
        <ContactForm />
        <Footer />
      </Container>
    </React.Fragment>
  );
}


export default Landing;