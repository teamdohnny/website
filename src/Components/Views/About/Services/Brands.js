import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import ItemsCarousel from 'react-items-carousel';

import Slider from "react-slick";


import {Container, Img} from 'local_npm/react-container';
import Typography from 'local_npm/react-styled-typography';


import BRANDS from "Components/Constants/ServicesDetails";

import MediaQuery from 'react-responsive'


import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

const noOfItems = 12;
const noOfCards = 5;
const autoPlayDelay = 2000;
const chevronWidth = 40;

const Wrapper = styled.div`
 
  max-width: 100%;
  margin: 0 auto;
`;

const SlideItem = styled(Container)`
	

  font-weight: bold;
  //background: orange;
  //border: solid 1px black;
`;

const carouselItems = _.map(BRANDS,(data,index) => (
  <SlideItem 
  center
  height="200px"
  tabletCSS="height:200px;"
  key={index}>
  <Container  height="100%"  css=" padding: 2rem;  margin: 0 auto;">

  <Container 
    height="66px" 
    >

     <Img
    		src={data.img}
    		width="56px"
    		margin="0.5rem 0"
  		/>
      
    </Container>


     <Typography 
          fontSize="20px"
          weight="600"
          css={` width: 90%; margin: 16.5px auto; line-height:30px;`}
          align="left"
          tabletCSS={`
            //font-size:45px;
            //line-height: 55px;
          `} 
          color="#212121">

         {data.title}
        </Typography>

          <Typography 
          fontSize="14px"
          weight="400"
          css={` width: 90%; margin: 0 auto; line-height:24px;`}
          align="left"
          tabletCSS={`
            //font-size:45px;
            //line-height: 55px;
          `} 
          color="#212121">

         {data.subtitle}
        </Typography>
			</Container>
  </SlideItem>
));

export default class AutoPlayCarousel extends React.Component {
  state = {
    activeItemIndex: 0,
  };

  componentDidMount() {
  //  this.interval = setInterval(this.tick, autoPlayDelay);

  //this.slider.slickPlay();
  }

  componentWillUnmount() {
  //  clearInterval(this.interval);
    //this.slider.slickPause();
  }

  tick = () => this.setState(prevState => ({
    activeItemIndex: (prevState.activeItemIndex + 1) % (noOfItems-noOfCards + 1),
  }));

  onChange = value => this.setState({ activeItemIndex: value });

  render() {


  const settings = {
  	pauseOnHover:false,
      dots: false,
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 1500
    };


      const settings2 = {
      	pauseOnHover:false,
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1500
    };

    return (
    	    <Container
	    		width="95%"
	    		css="margin: 50px auto;">
			<MediaQuery minDeviceWidth={791}>

			 <Slider {...settings}  children={carouselItems} />

			</MediaQuery>


        <MediaQuery maxDeviceWidth={950}>
         <Slider {...settings2}  children={carouselItems} />

        </MediaQuery>
      </Container>
    );
  }
}
