import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';

const Brands = () => (
	<React.Fragment>

	    <Container 
	    	row 
	    	width="95%" 
	    	margin="110px auto" 
	    >
			
			<Img 
				src={require("static/pictures/brands/b1.png")}  
				img={{css:"width:45%;", tabletCSS:"width:75%;"}} 
				width="32%"
				tabletCSS={"width: 50%;"}
				
				margin="0.5rem 0" 
				height="75px" 
				
			/>

			<Img 
				src={require("static/pictures/brands/b2.png")}  
				img={{css:"width:45%;", tabletCSS:"width:75%;"}} 
				width="32%"
				tabletCSS={"width: 50%;"}
				
				margin="0.5rem 0" 
				height="75px" 
				
			/>

			<Img 
				src={require("static/pictures/brands/b3.png")}  
				img={{css:"width:45%;", tabletCSS:"width:75%;"}} 
				width="32%"
				tabletCSS={"width: 50%;"}
				
				margin="0.5rem 0" 
				height="75px" 
				
			/>

			<Img 
				src={require("static/pictures/brands/b4.png")}  
				img={{css:"width:75%;"}} 
				width="33%"
				tabletCSS={"width: 50%; display: inline-block;"}
				css="display: none;"
				margin="0.5rem 0" 
				height="75px" 
				overflowHidden
			/>
			
			

	    </Container>
	</React.Fragment>
);

export default Brands;