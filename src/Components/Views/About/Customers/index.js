import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"

import {  Link} from 'react-router-dom';

import Cover from "Components/Molecules/Stories"
import TopBrands from "./TopBrands"
 import Fade from 'react-reveal/Fade';
 
const Landing = () => (
   <React.Fragment>
<Cover
  video="https://www.youtube.com/embed/KwtH6_n2MfQ"
  title={`"Hemos logrado contar con una plataforma que brinda valores agregados a nuestra organización"`}
  person=" MBA. Javier Rodriguez"
  img={`"${require("static/pictures/04. Historias de éxito/Historia 1.png")}"`}

 />
<Fade bottom>
    <Container row width="85%" margin="0 auto" css="margin-top:110px; ">
      <Container 
        width="60%"  
        
        tabletCSS={`
            ${"width:100%;"}
          `} 
         >
        <Typography 
          fontSize="60px"
  
          weight="500"
           tabletCSS="font-size: 35px; line-height:45px; margin-bottom: 50px;"
          color="#000">
         Bridando soluciones con éxito desde 1999.
        </Typography>
      </Container>

      <Container 
        width="30%" 
        
        tabletCSS={`
            ${"width:100%;"}
          `}>

        <Typography 
          fontSize="22px"
          weight="400"
          marginB="10px"
           tabletCSS="font-size: 16px; line-height:26px;"
        css="line-height: 32px;"
      
          color="#000">
         Hemos colaborado con más de 500 organizaciones en fortalecer y mejorar sus procesos a través de nuestros productos y servicios.
            <br/><br/><br/>
            <Link to="/about">
            <Button transparent >
              <Container center row style={{fontWeight:"500"}}><Typography weight="500" align="left" marginR="10"> ¿Por qué nosotros? </Typography> <ArrowIcon style={{fontSize:"26px"}}/>  </Container></Button>
          </Link>
          </Typography>

      </Container>
    </Container>
</Fade>
     

    </React.Fragment>
);

export default Landing;