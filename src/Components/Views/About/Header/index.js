import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/clay-components';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {  Link} from 'react-router-dom';
 import SharpPlayArrow from "react-md-icon/dist/SharpPlayArrow";
 import Fade from 'react-reveal/Fade';


const Landing = () => (
   <React.Fragment>
    <Fade cascade big>
    <Container  
      width="80%" 
      margin="0 auto" 
      css="margin-top:170px; "   
      tabletCSS={`
         width: 90%;
         padding-right: 10%;
         margin-top: 100px;
          `}>

      <Container 
        width="100%"  
        center
        css="text-align:center;"
        tabletCSS={`
            width:100%;
          `} 
         >

        <Typography 
          fontSize="70px"
          weight="600"
          css={` width: 100%; margin: 0 auto; line-height:80px`}
          align="center"
          tabletCSS={`
            font-size:45px;
            line-height: 55px;
            text-align: left;
          `} 
          color="#212121">

        Llevamos las experiencias digitales al siguiente nivel.
        </Typography>
      </Container>

    </Container>
</Fade>



 <Container
        corner="10px"
        overflowHidden
        width="90%"
        margin="100px auto"
        background="#F8F9FA"
        css={`
          padding:2.5rem;
          margin-bottom:50px;
          border: 1px solid #E3E4E7; 
          .icon{
            display:none;
          } 

          
          &:hover{
            
            .icon{
              display:inline-block;
          }}`}
       >

       <Container row>


       {[
        {title:"Clientes", extra:"+10", subtitle:"Han confiado en nosotros para construir sus productos."},
         {title:"Pantallas", extra:"+90", subtitle:"Diseñadas para todos los dispositivos en formatos web y móvil."},
          {title:"Años", extra:"5", subtitle:"Trabajando en la industria, aprendiendo y mejorando practicas."},
           {title:"Verticales", extra:"6", subtitle:"Finanzas, administración, legal, Blockchain y Programación,"}



        ].map((item, key)=><Container width="23%" tabletCSS="width:100%; margin-bottom: 2rem;">

             <Typography 
          fontSize="50px"
          weight="600"
          css={` width: 100%; margin: 0 auto; line-height:60px`}
          align="left"
          tabletCSS={`
            //font-size:45px;
           // line-height: 55px;
          `} 
          color="#212121">

       {item.extra}
        </Typography>

           <Typography 
          fontSize="30px"
          weight="600"
          css={` width: 100%; margin: 0 auto; line-height:40px`}
          align="left"
          tabletCSS={`
            //font-size:45px;
            //line-height: 55px;
          `} 
          color="#212121">

       {item.title}
        </Typography>

           <Typography 
          fontSize="14px"
          weight="400"
          css={` width: 100%; margin: 0 auto; line-height:24px`}
          align="left"
          tabletCSS={`
            
          `} 
          color="#212121">

        {item.subtitle}
        </Typography>
          



        </Container>)}


  
</Container>

</Container>


    </React.Fragment>
);

export default Landing;