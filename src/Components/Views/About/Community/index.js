import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/clay-components';
import {Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {  Link} from 'react-router-dom';
 import SharpPlayArrow from "react-md-icon/dist/SharpPlayArrow";

import Brands from "./Brands"
import Gallery from "./Gallery"
 import Fade from 'react-reveal/Fade';

import StoryCard from "Components/Molecules/StoryCard"


const Landing = () => (
   <React.Fragment>
    <Fade cascade big>
    <Container  
      width="85%" 
      margin="0 auto" 
      css="margin-top:170px; "   
      tabletCSS={`
           width: 90%;
           padding-right: 5%;
           margin-top: 100px;
      `}>

      <Container 
        width="100%"  
        center
      css="text-align:center;"
        tabletCSS={`
            width:100%;
          `} 
         >

        <Typography 
          fontSize="70px"
          weight="600"
          css={` width: 100%; margin: 0 auto; line-height:80px;`}
          align="center"
          tabletCSS={`
            font-size:45px;
            line-height: 55px;
            text-align: left;
          `} 
          color="#212121">

       Somos parte de la mejor comunidad de México. 
        </Typography>
      </Container>

    </Container>
</Fade>
<Container width="90%" margin="0 auto">
<Brands />
<Gallery />
</Container>


    </React.Fragment>
);

export default Landing;