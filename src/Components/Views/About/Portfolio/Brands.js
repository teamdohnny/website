import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import ItemsCarousel from 'react-items-carousel';

import Slider from "react-slick";


import {Container, Img} from 'local_npm/react-container';

import BRANDS from "Components/Constants/TechIcons";

import MediaQuery from 'react-responsive'


import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

const noOfItems = 12;
const noOfCards = 5;
const autoPlayDelay = 2000;
const chevronWidth = 40;

const Wrapper = styled.div`
 
  max-width: 100%;
  margin: 0 auto;
`;

const SlideItem = styled(Container)`
	


 
  font-weight: bold;
  //background: orange;
  //border: solid 1px black;
`;

const carouselItems = _.map(BRANDS,(data,index) => (
  <SlideItem 
  center
  height="200px"
  tabletCSS="height:200px;"
  key={index}>
  <Container center height="100%">

   <Img 

				src={data.img}  
				 
				width="58%" 
				tabletCSS={"width: 52%;"} 
				margin="0.5rem 0" 
				
				
			/>
			</Container>
  </SlideItem>
));

export default class AutoPlayCarousel extends React.Component {
  state = {
    activeItemIndex: 0,
  };

  componentDidMount() {
  //  this.interval = setInterval(this.tick, autoPlayDelay);

  //this.slider.slickPlay();
  }

  componentWillUnmount() {
  //  clearInterval(this.interval);
    //this.slider.slickPause();
  }

  tick = () => this.setState(prevState => ({
    activeItemIndex: (prevState.activeItemIndex + 1) % (noOfItems-noOfCards + 1),
  }));

  onChange = value => this.setState({ activeItemIndex: value });

  render() {


  const settings = {
  	pauseOnHover:false,
      dots: false,
      infinite: true,
      slidesToShow: 8,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 1500
    };


      const settings2 = {
      	pauseOnHover:false,
      dots: false,
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 1500
    };





    return (
    	    <Container
	    		width="95%"
	    		css="margin: 50px auto;">
			<MediaQuery minDeviceWidth={791}>





			
			 <Slider {...settings}  children={carouselItems} />


			
     


			{/*	<ItemsCarousel
							infiniteLoop={true}
							gutter={14}
							numberOfCards={noOfCards}
							activeItemIndex={this.state.activeItemIndex}
							requestToChangeActive={this.onChange}
			
							chevronWidth={chevronWidth}
							outsideChevron
							children={carouselItems}
							/>*/}
			</MediaQuery>


        <MediaQuery maxDeviceWidth={950}>
         <Slider {...settings2}  children={carouselItems} />
      {/*  <ItemsCarousel
              infiniteLoop={true}
                gutter={14}
                numberOfCards={2}
                activeItemIndex={this.state.activeItemIndex}
                requestToChangeActive={this.onChange}
                chevronWidth={chevronWidth}
                outsideChevron
                children={carouselItems}
              />*/}
        </MediaQuery>
      </Container>
    );
  }
}
