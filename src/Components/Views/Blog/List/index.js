import React from 'react';
import {Container} from 'local_npm/clay-components';
import BlogCard from "Components/Molecules/BlogCard"
import ShooterGenericList from "Components/Molecules/ShooterGenericList"

const StoriesList = () => (
  <React.Fragment>
      <Container 
        wrapper={{width:"90%", margin:"180px auto", tabletCSS: `margin: 160px auto; `}}  
        css="padding:-1rem;" 
        margin="50px auto" 
        corner="10px" 

        row>


      <ShooterGenericList 
        Component={BlogCard} 
        query={{params:{type:"blog"}}} />


    </Container>
  </React.Fragment>
);

export default StoriesList;