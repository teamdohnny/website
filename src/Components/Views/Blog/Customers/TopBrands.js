import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';


const Landing = () => (
   <React.Fragment>

    <Container row width="90%" margin="110px auto" >
		<Img background="gray" img={{css:"opacity:0.12;"}} width="32%" margin="0.5rem 0" height="75px" overflowHidden/>
		<Img background="gray" img={{css:"opacity:0.12;"}} width="32%" margin="0.5rem 0" height="75px" overflowHidden/>
		<Img background="gray" img={{css:"opacity:0.12;"}} width="32%" margin="0.5rem 0" height="75px" overflowHidden/>
	
    </Container>

    </React.Fragment>
);

export default Landing;