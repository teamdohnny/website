import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, PlayIcon} from "Components/Primitives/Button"

const img = "https://images.unsplash.com/photo-1556741568-055d848f8bfd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80"
const Landing = () => (
   <React.Fragment>

    <Container 
      overflowHidden  
      backgroundImg={img} 
      width="100%" 
      height="680px" 
      css="margin-top:110px; position: relative; ">
    
     <Container  
      width="100%" 
      height="100%" 
      background="rgba(0,0,0,0.4)" 
      css="position: absolute; z-index:0;"/>
       
       <Container 
       
       margin="" width="30%" 
       tabletCSS="width: 80%; font-size: 35px; line-height:45px; top: auto; bottom: 50px;  left:5%;"
       css=" z-index:1; position: absolute; top: 200px; left:7.3%;">
        <Typography 
          tabletCSS="font-size: 35px; line-height:45px;"

        fontSize="50px" color="white" align="left">
        "Su sistema, una parte integral de nuestra Institución.”
        </Typography>
          <Typography 
            tabletCSS="font-size: 20px; line-height:30px; "
          fontSize="22px" color="white" align="left" marginT="40" marginB="40">
        - Caloncho López
        </Typography>
<Container css="text-align:left; margin-left:-0.78rem;">
<Button transparent style={{color:"white", "margin":"0"}}>
<Container 

center
 row >

 <PlayIcon style={{fontSize:"58px", marginLeft:"-11%;"}}/>   
 <Typography   
 tabletCSS="font-size: 35px; line-height:45px; margin:0; " 
 fontSize="42px" 
 align="left" 
 marginR="10"> Ver testimonio</Typography>   
 </Container></Button>
</Container>
      </Container>
    </Container>

    </React.Fragment>
);

export default Landing;