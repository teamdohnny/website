import React  from 'react';
import Navbar from "Components/Molecules/BlogNavbar"
import BaseNavbar from "Components/Molecules/Navbar"
import ReturnButton from "Components/Molecules/ReturnButton"
import Footer from "Components/Molecules/Footer"
import ContactForm from "Components/Molecules/ContactForm"
import List from "./List"
import Customers from "./Customers"
import styled from 'styled-components';
import {useShooterGetItem} from "Components/Hooks/useShooterAPI";
import {Container} from "local_npm/clay-components";
import {bodyStyle} from "clay-styles"



const Blog = () => {

  return(
    <React.Fragment>
    <Container css="display: none;" tabletCSS="display: inline-block;">
      <BaseNavbar noshadow/>
      </Container>
      <Navbar returnTo="/" />
       <Container {...bodyStyle}>

      <Container tabletCSS={`display: none;`}>
        <ReturnButton to="/"/>
        </Container>
        <List />
   
        <ContactForm />
        <Footer />
      </Container>
    </React.Fragment>
  );
}

export default Blog;