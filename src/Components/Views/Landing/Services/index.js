import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/clay-components';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {  Link} from 'react-router-dom';
import SharpPlayArrow from "react-md-icon/dist/SharpPlayArrow";
import Brands from "Components/Molecules/Brands"
import Fade from 'react-reveal/Fade';
import ServiceCard from "Components/Molecules/ServiceCard"
import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";


const Services = (props) => (
   <React.Fragment>
    <Fade 
      cascade 
      big>

    <Container  
      width="85%" 
      margin="0 auto" 
      css="margin-top:170px; "


      tabletCSS={`
          margin-top:100px;
          width: 90%;

        `}



      >

      <Container 
        width="100%"  
        center
        css="text-align:center;"
        tabletCSS={`
            width:100%;
          `} 
         >

        <Typography 
          fontSize="80px"
          weight="600"
          css={` width: 100%; margin: 0 auto;`}
          align="center"
          tabletCSS={`
            text-align: left;
            font-size:45px;
            line-height: 55px;
          `} 
          color="#212121">

         Lo que puedes hacer con nosotros.
        </Typography>
      </Container>

    </Container>
</Fade>


<Container 
  wrapper={{width:"90%", margin:"0 auto"}}  
  css="padding:-1rem;" 
  margin="120px auto" 
  tabletCSS={`margin: 50px auto;`}
  corner="10px" 
  row>

  <ServiceCard title="Auditoria UI/UX" delay={1}/>
  <ServiceCard title="Estrategia de producto" delay={2}/>
  <ServiceCard title="Metodologías agiles" delay={3}/>
  <ServiceCard title="Branding" delay={4}/>
  <ServiceCard title="Diseño UI/UX" delay={5}/>
  <ServiceCard title="Prototipos" delay={6}/>
  <ServiceCard title="Sitios web" delay={7}/>
  <ServiceCard title="Aplicaciones web" delay={8}/>
  <ServiceCard title="PWA" delay={9}/>


</Container>
 <Link 
  to="/services" 
  style={{textDecoration:"none"}}>
    <Container 
      center 
      height="110px;">
     
    <Container 
        row 
        width="20%"
        css="text-decoration: none;"
        tabletCSS={"width:90%;"}>


    <Container 
        width="80%" 
        css={`
          font-weight:500; 
          font-size: 18px; 
          color:#677282; 
          text-decoration: none;`}>

    Leer más en servicios
    </Container>



 


    <Container width="20%" css="font-weight:500; font-size: 24px; color:#677282;">
     <ArrowIcon style={{color:"#677282"}}/>
    </Container>
    </Container>
    </Container>
</Link>

   <br/>
     <br/>

 <br/>

 <br/>

 <br/>
  <br/>

 <br/>

 <br/>
  <br/>


 <br/>
</React.Fragment>
);

export default Services;