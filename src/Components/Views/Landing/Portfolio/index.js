import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/clay-components';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {  Link} from 'react-router-dom';
import SharpPlayArrow from "react-md-icon/dist/SharpPlayArrow";
import Brands from "Components/Molecules/Brands"
import Fade from 'react-reveal/Fade';
import StoryCard from "Components/Molecules/StoryCard"
import ShooterGenericList from "Components/Molecules/ShooterGenericList"


const Landing = () => (
   <React.Fragment>
    <Fade cascade big>
    <Container  
      width="85%" 
      margin="0 auto" 
      css="margin-top:170px; "

      tabletCSS={`

            margin-top:100px;
            width:90%;
          `} 



      >

      <Container 

        width="100%"  
        center
      css="text-align:center;"
        tabletCSS={`
            width:100%;
          `} 
         >

        <Typography 
          fontSize="70px"
          weight="600"
          css={` width: 100%; margin: 0 auto; line-height:80px;`}
          align="center"
          tabletCSS={`

            text-align:left;
            font-size:45px;
            line-height: 55px;
          `} 
          color="#212121">
         Llevamos experiencias digitales al siguiente nivel.
        </Typography>
      </Container>
    </Container>
  </Fade>

  <Brands />

  <Container 
    wrapper={{width:"90%", margin:"0 auto"}} 
    css="padding:-1rem;" 
    margin="50px auto" 
    corner="10px" 
    row>

    <ShooterGenericList
      Component={StoryCard} 
      query={{params:{type:"story", limit:4}} }/>


  </Container>



  <Link to="/stories" style={{textDecoration:"none"}}>


    <Container 
      center 
      height="210px;">

    <Container 
      row 
      width="30%"
      css="text-decoration: none;"
      tabletCSS={"width:90%;"}>


    <Container
    center
      tabletCSS={"font-size: 14px; width: 85%;"}
      width="80%" 
      css={`
        font-weight:500; 
        font-size: 16px; 
        color:#677282; 
        text-decoration: none;`}>

    Mira todos nuestros casos de éxito
    </Container>

    <Container 
       height="210px;"
       center
      width="20%"  
      tabletCSS={"width: 15%;"} 
      css="font-weight:500; font-size: 24px; color:#677282;">
    <ArrowIcon style={{color:"#677282"}}/>
    </Container>
    </Container>
    </Container>

    </Link>
    <br/>
    <br/>
    <br/>
    <br/>
  </React.Fragment>
);

export default Landing;