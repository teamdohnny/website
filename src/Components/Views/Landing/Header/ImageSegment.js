import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';

const IMGS = [

    require("static/pictures/01. Landing/1.jpg"),
    require("static/pictures/01. Landing/2.jpg"),
    require("static/pictures/01. Landing/3.jpg"),
    require("static/pictures/01. Landing/4.jpg")

];

const Landing = () => (
   <React.Fragment>
   <Container overflowHidden>
    <Container 

    row width="101%" margin="0 -0.5%" css="margin-top:110px; ">
     <Container

     tabletCSS={`

            width:48%;
            height: 185px;
            margin-top: 1rem;
          `} 
        backgroundImg={`"${IMGS[0]}"`}
        width="23%" 
        height="355px" 
        overflowHidden/>
     <Container
     tabletCSS={`
            width:48%;
            height: 185px;
            margin-top: 1rem;
          `} 

   backgroundImg={`"${IMGS[1]}"`}
     width="23%" height="355px" overflowHidden />
     <Container
     tabletCSS={`
            width:48%;
            height: 185px;
            margin-top: 1rem;
          `} 
    backgroundImg={`"${IMGS[2]}"`}
    width="23%" height="355px" overflowHidden/>
     <Container
     tabletCSS={`
            width:48%;
            height: 185px;
            margin-top: 1rem;
          `} 
   backgroundImg={`"${IMGS[3]}"`}
     width="23%" height="355px" overflowHidden/>
    </Container>
    </Container>

    </React.Fragment>
);

export default Landing;