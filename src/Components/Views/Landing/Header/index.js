import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container} from 'local_npm/clay-components';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import {  Link} from 'react-router-dom';
 import SharpPlayArrow from "react-md-icon/dist/SharpPlayArrow";
 import Fade from 'react-reveal/Fade';


const Landing = () => (
   <React.Fragment>
    <Fade cascade big>
    <Container  width="80%" margin="0 auto" css="margin-top:170px; " tabletCSS={`margin-top: 100px; width:90%;`}>

      <Container 
        width="100%"  
        center
      css="text-align:center;"
        tabletCSS={`
            width:100%;
          `} 
         >

        <Typography 
          fontSize="70px"
          weight="600"
          css={` width: 100%; margin: 0 auto; line-height: 80px;`}
          align="center"
          tabletCSS={`
            text-align: left;
            font-size:45px;
            line-height: 55px;
          `} 
          color="#212121">

         Te ayudamos a superar desafíos con diseño y tecnología.
        </Typography>
      </Container>

    </Container>
</Fade>


<Container 
  overflowHidden 
  width="90%" 
  margin="140px auto" 
  corner="10px" 
  background="#F8F9FA"   
  backgroundImg={`"https://cdn.dribbble.com/users/2587756/screenshots/7824056/media/9a38d0bd26d94d823c9768421665eb06.jpg"`} 
  height="600px" 
  center
  tabletCSS={`margin: 100px auto; height:400px;`}
  >
 

 <Container css={`
  background:black;
  opacity: 0.3;
  top:0;
  left:0;
  bottom:0;
  left:0;

  `} position="absolute"/>
  <Container circle="100px" center defaultShadow background="white" css="cursor:pointer;">

<SharpPlayArrow style={{fontSize: "42px"}}/>
  </Container>

</Container>


    </React.Fragment>
);

export default Landing;