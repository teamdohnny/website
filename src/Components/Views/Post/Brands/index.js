import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import Brands from "Components/Molecules/Brands"

const Landing = () => (
   <React.Fragment>

    <Container  
      width="90%" 
      margin="150px auto">
      
        <Typography 
          fontSize="50px"
          center
          weight="400"
          css="margin: 0 auto; width:100%; text-align:center;"
          tabletCSS={`
        
            font-size:35px; 
            line-height:45px;
           
            `} 
          color="#000">Más de 500 empresas confían en nosotros.
        </Typography>
  
   <Brands />
      
    </Container>
 
   

    </React.Fragment>
);

export default Landing;