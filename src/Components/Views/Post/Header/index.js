import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button"
import SharpShare from "react-md-icon/dist/SharpShare";
import SharpClose from "react-md-icon/dist/SharpClose";
import SharpArrowBack from "react-md-icon/dist/SharpArrowBack";
import {Link} from "react-router-dom"
import _ from "lodash"

import Fade from 'react-reveal/Fade';


import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
   FacebookIcon,
  TwitterIcon,
  TelegramIcon,
  WhatsappIcon,
  LinkedinIcon,
} from 'react-share';



const ReactMarkdown = require('react-markdown')






const Share = (id)=>{


  const [open, setOpen] =  React.useState(false)


  if(open)
     return(<Container
            
            width="200px"
            defaultCorner
            background="white" 
            shadow="0 0.5px 3px rgba(0,0,0,0.4)" 
        
             
            css={`

              position:absolute; 
              bottom:-100px; 
              right: 10%;
              cursor: pointer;
              &:hover{
                box-shadow:0 0.3px 8px rgba(0,0,0,0.5);

              }
              padding: 1rem;


              `} >


              <Container row css="position: relative; margin-bottom: 30px; ">

              <Typography 
                marginT="2"
                weight="500"
                fontSize="16px"
                color="#212121">
                Compartir
              
              </Typography>
                

              </Container>

              <FacebookShareButton url={"http://siacpage.herokuapp.com/post/"+ id}>
              <Container row width="80%"  margin="1rem 0">
                <Container width="20%"  height="30px" center>
                <Img src={require("static/pictures/iconos/Facebook.svg")}/>
                </Container>
                <Container width="80%" height="30px" center>
                        <Container css="padding-left: 2rem;">
                  <Typography color="#677282" fontSize="14px" weight="500"  >Facebook</Typography>
                    </Container>
                </Container>
              </Container>
               </FacebookShareButton>

               <TwitterShareButton url={"http://siacpage.herokuapp.com/post/"+ id}>
             <Container row width="80%"  margin="1rem 0">
                <Container width="20%"  height="30px" center>
                <Img src={require("static/pictures/iconos/Twitter.svg")}/>
                </Container>
                <Container width="80%" height="30px" center><Container css="padding-left: 2rem;">
                  <Typography color="#677282" fontSize="14px" weight="500"  >Twitter</Typography></Container>
                </Container>
              </Container>
               </TwitterShareButton>


               <LinkedinShareButton url={"http://siacpage.herokuapp.com/post/"+ id}>
              <Container row width="80%"  margin="1rem 0">
                <Container width="20%"  height="30px" center>
                <Img src={require("static/pictures/iconos/Linkedin.svg")}/>
                </Container>
                <Container width="80%" height="30px" center><Container css="padding-left: 2rem;">
                  <Typography color="#677282" fontSize="14px" weight="500"  >Linkedin</Typography></Container>
                </Container>
              </Container>
               </LinkedinShareButton>



               <SharpClose 
                onClick={()=>{setOpen(false)}} 

               style={{color:"black", position:"absolute", top: "1rem", right:"1rem", fontSize:"24px"}}/>

              </Container>)

    return(<Container
            onClick={()=>{setOpen(true)}} 
            circle="57px" 
            background="white" 
            shadow="0 0.5px 3px rgba(0,0,0,0.4)" 
            
            center 
            css={`
              position:absolute; 
              bottom:-20.5px; 
              right: 10%;
              cursor: pointer;
              &:hover{
                box-shadow:0 0.3px 8px rgba(0,0,0,0.5);
              }
              `} ><SharpShare style={{color:"black", fontSize:"24px"}}/></Container>)

}



const Landing = (props) => (
   <React.Fragment>
<Fade>


    <Container  width="85%" margin="100px auto" css="position: relative; margin-top: 245px;" tabletCSS="width: 90%;">

    
<Link to="/blog">
    <Button transparent style={{position: "absolute", left: "0", top: "-180px",  color:"#677282"}}> 
    <Container 
      center 
      row 
      style={{fontWeight:"500"}}>
      <SharpArrowBack style={{fontSize:"24px"}}  />
      
      <Typography 
        fontSize="16px"
        weight="500" 
        align="left" 
        marginL="10"> 
          Regresar
        </Typography> 
    
        </Container>
      </Button>
  </Link>


      <Container 
          width="85%" 
        margin="0 auto"   
        
        tabletCSS={`
            ${"width:100%;"}
          `} 
         >
        <Typography 
          fontSize="80px"
          
          weight="500"
          marginB="25"
         tabletCSS={`
        
            font-size:45px; 
            line-height:55px;
           
            `}  
          color="#000">


    {_.get(props, "data.titulo")}


        </Typography>
      </Container>
 
      <Container 
          width="85%" 
        margin="0 auto" 
        
        tabletCSS={`
            width:100%;
          `}>

        <Typography 
          fontSize="25px"
          weight="400"
          marginB="10px"
        css="line-height: 35px;"
         tabletCSS={`
            margin-top: 40px;
            font-size:18px; 
            line-height:28px;
            width: 100%;
            `} 
          color="#677282">

         {_.get(props, "data.descripcion")}
        </Typography>

      </Container>

      <Container   css="margin-top: 100px; position: relative;" tabletCSS="width:112%; margin-left: -6%;" >
      
        <Img 
            img={{background:"#0035C9"}}
            src= {_.get(props, "data.cover")} 
            />

            <Share id={props.id}/>



      </Container>
    </Container>

   
</Fade>


    <Container    margin="100px auto" css="width:50%; text-aling:left;" tabletCSS="width:90%;">
    

    <Container 


    css={`

      *{
        text-align: left;
        color: #677282;
        font-size: 16px;
        line-height: 26px;

      }
      h1,h2,h3,h4,h5,h6{
        color:#212121;
        font-weight: 500;
        
      }


      h1{

        font-size: 35px;
        line-height: 45px;



      }

      h2{

        font-size: 30px;
        line-height: 40px;


      }

      h3{
        font-size: 25px;
        line-height: 35px;


      }

      h4{font-size: 20px;
        line-height: 20px;}

      h5{

        font-size: 16px;
        line-height: 16px;



      }

      h6{

        font-size: 14px;
        line-height: 14px;


      }

      img{
        width: 100%;
      }


      `}
      >
      <ReactMarkdown source={_.get(props, "data.historia.markdown")}  />


            </Container>
       </Container>

    </React.Fragment>
);

export default Landing;