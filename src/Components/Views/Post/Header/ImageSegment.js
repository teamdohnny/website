import React from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img} from 'local_npm/react-container';


const Landing = () => (
   <React.Fragment>
   <Container overflowHidden>
    <Container row width="101%" margin="0 -0.5%" css="margin-top:110px; ">
     <Img background="red" img={{css:"opacity:0.8;"}}width="23%" height="355px" overflowHidden/>
     <Img width="23%" height="355px" overflowHidden />
     <Img width="23%" height="355px" overflowHidden/>
     <Img width="23%" height="355px" overflowHidden/>
    </Container>
    </Container>

    </React.Fragment>
);

export default Landing;