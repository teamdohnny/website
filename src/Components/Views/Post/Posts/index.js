import React, {useEffect} from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img,StyledContainer} from 'local_npm/react-container';

import {Button, ArrowIcon} from "Components/Primitives/Button";


import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";


import Bounce from 'react-reveal/Bounce';
import withReveal from 'react-reveal/withReveal';



const Landing = () => {

    const Animation1 = withReveal (Container ,<Bounce cascade bottom delay={50} /> )
    const Animation2 = withReveal (Container ,<Bounce cascade bottom delay={100} /> )
    const Animation3 = withReveal (Container ,<Bounce cascade bottom delay={150} /> )


    return (
   <React.Fragment>


    <Container
        tabletCSS="width:90%;" 
        width="85%" 
        margin="110px auto">

       <Typography 
            fontSize="60px" 
            color="black"
            weight="500"
            marginB="20"
            css={`line-height:70px;`}>
   Todas las entradas.
        </Typography>

        </Container>
   
    <Container
        row
        tabletCSS="width:90%;" 
        width="85%" 
        margin="110px auto">


    <Container
        css={`

            display: inline-block;
           
            padding:1rem;
        `} 
        tabletCSS="width:100%;">



        <Animation2
           
         
            key="12" 
            border="1px solid #212121"
       
            overflowHidden
            css={`padding:2rem; position: relative;`}>
            <Container row>

        <Img
            width="20%"
            background="#EAEAEA"
            src=""
            tabletCSS={"width:90%; margin: 0 auto;"}
            css="padding: 2.5rem;"
          
            overflowHidden/>


    <Container width="77%">

        <Typography
            align="left"
            weight="500"
            fontSize="35px"
            css={`line-height:45px;`}
            marginB="20">

            El futuro de las microfinanzas

        </Typography>

        <Typography 
            fontSize="18px" 
            color="#677282"
            marginB="20"
            css={`line-height:28px;`}>
     La simplicidad y el mejor UX del mundo llego a SIAC, en plena mitad de año, rediseñamos la marca SIAC. Pero, nuestra misión es la misma: Potencializar empresas de finanzas, llevarlas al otro nivel con tecnología.
        </Typography>


       
            </Container>

            </Container>
            </Animation2>

        </Container>        
    </Container>


    <Container 
    center 
    height="100px" 
    css="margin-bottom: 100px; cursor:pointer;" 
    color="#212121"> 


<Container 
    row 
    width="10%"

    tabletCSS={"width:90%; margin: 0 auto;"}

     >


<Container 
    width="80%" 
    css="font-weight:500; font-size: 18px;">
Mostrar más
</Container>

<Container width="20%" css="font-weight:500; font-size: 24px;">
 <BaselineKeyboardArrowDown/>
</Container>




</Container>


    </Container>





    </React.Fragment>
);}

export default Landing;