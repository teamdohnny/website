import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img,StyledContainer} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button";
import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";
import Bounce from 'react-reveal/Bounce';
import withReveal from 'react-reveal/withReveal';
import {Link} from "react-router-dom"
import axios from "axios"
import _ from "lodash"
import BlogCard from "Components/Molecules/BlogCard"
import ShooterGenericList from "Components/Molecules/ShooterGenericList"


const Recomendations = ({id}) => {



return (
   <React.Fragment>
    <Container
        tabletCSS="width:90%;" 
        width="85%" 
        margin="50px auto">

       <Typography 
            fontSize="30px" 
            color="black"
            tabletCSS="font-size: 16px; line-height: 26px; font-weight: 500;"
            weight="600"
            marginB="20"
            css={`line-height:35px;`}>
    Lee otros casos de éxito    
        </Typography>


        <Container >
      

     <ShooterGenericList 
          limit = {2}
          Component={BlogCard} 
          query={{params:{type:"blog", limit:3}, omit:{key:"id", value:id}}} />



</Container>
</Container>
    

    </React.Fragment>
);}

export default Recomendations;
