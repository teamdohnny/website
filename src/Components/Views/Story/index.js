import React, {useState, useContext, useEffect} from 'react';
import Navbar from "Components/Molecules/StoriesNavbar"; 
import Footer from "Components/Molecules/Footer";
import ContactForm from "Components/Molecules/ContactForm";
import ShooterTemplate from "Components/Molecules/ShooterTemplateBasicV1";
import Recomendations from "./Recomendations";
import {useShooterGetItem} from "Components/Hooks/useShooterAPI";
import {Redirect} from "react-router-dom";
import {Container} from "local_npm/react-container";
import styled from 'styled-components';
import BaseNavbar from "Components/Molecules/Navbar"
//import Bottom from "Components/Molecules/Bottom"

import {bodyStyle} from "clay-styles"
 
export const Body = styled.div`
  max-width: 1400px;
  width: 90%;
  background: white;
  margin: 0 auto;
  overflow: hidden;
  text-align: center;
  box-shadow: 0px 0.5px 3px #00000033;
`;
 

const Story = (props) => {
  const {
    item, 
    loadingItem, 
    errorItem
  } = useShooterGetItem({params:{
      type:"story", 
      id: props.match.params.id
    }});

 if(errorItem)
  return <Redirect to="/404"/>


 if(!loadingItem && !item)
    return <Redirect to="/404"/>

  return(
    <React.Fragment>
        <Container css="display: none;" tabletCSS="display: inline-block;">
      <BaseNavbar noshadow/>
      </Container>
      <Navbar story />
      <Container {...bodyStyle}>
        {loadingItem?
          <Container height="90vh" center>
            <h1>Cargando...</h1>
          </Container>:
          <React.Fragment>
            <ShooterTemplate     returnTo={"/stories"} {...item}/>
            <Container height="1px" background="rgba(0,0,0,0.2)" />
            <Recomendations 

            id={props.match.params.id} />
          </React.Fragment>
        }
      <ContactForm />
      <Footer />
      </Container>
    </React.Fragment>
  );

}
export default Story;
