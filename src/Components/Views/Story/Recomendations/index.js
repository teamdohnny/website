import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import Typography from 'local_npm/react-styled-typography';
import {Container, Img,StyledContainer} from 'local_npm/react-container';
import {Button, ArrowIcon} from "Components/Primitives/Button";
import BaselineKeyboardArrowDown from "react-md-icon/dist/SharpKeyboardArrowDown";
import Bounce from 'react-reveal/Bounce';
import withReveal from 'react-reveal/withReveal';
import {Link} from "react-router-dom"
import axios from "axios"
import _ from "lodash"
import StoryCard from "Components/Molecules/StoryCard"
import ShooterGenericList from "Components/Molecules/ShooterGenericList"


const Recomendations = ({id}) => {



return (
   <React.Fragment>
    <Container
        tabletCSS="width:90%;" 
        width="90%" 
        margin="50px auto">

       <Typography 
            fontSize="30px" 
            color="black"
            weight="600"
            tabletCSS="font-size: 16px; line-height: 26px; font-weight: 500;"
            marginB="20"
            css={`line-height:35px;`}>
    Lee otros casos de éxito    
        </Typography>


      <Container 
    
    css="padding:-1rem;" 
    margin="50px auto" 
    row>

     <ShooterGenericList 
        limit={2}
        Component={StoryCard} 
        query={{params:{type:"story", limit:3}, omit:{key:"id", value:id}}} />



</Container>
</Container>
    

    </React.Fragment>
);}

export default Recomendations;



/*
    <Container 
    center 
    height="100px" 
    css="margin-bottom: 100px; cursor:pointer;" 
    color="#212121"> 


<Container 
    row 
    width="10%" 
        tabletCSS={"width:90%; margin: 0 auto;"}
    >


<Container 
    width="80%" 
    css="font-weight:500; font-size: 18px;">
Mostrar más
</Container>

<Container width="20%" css="font-weight:500; font-size: 24px;">
 <BaselineKeyboardArrowDown/>
</Container>




</Container>


    </Container>*/