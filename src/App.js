
import React, { Component, useState, useEffect } from 'react';
import Landing from 'Components/Views/Landing';
import Error404 from 'Components/Views/Error404';
//import Privacy from 'Components/Views/Privacy'
import Stories from 'Components/Views/Stories';
//import Terms from 'Components/Views/Terms';
import About from 'Components/Views/About';
import Blog from 'Components/Views/Blog';
import Services from 'Components/Views/Services';
import Post from 'Components/Views/Post';
import Story from 'Components/Views/Story';
import UserForm from 'Components/Views/UserDataForm';


import scrollToTopOnMount from 'Files/General/scrollToTopOnMount';
import { Route, Switch } from 'react-router-dom';

export const ModalsContext = React.createContext();

//import { loadReCaptcha } from 'react-recaptcha-v3'
//import Cookies from 'Components/Molecules/Cookies';
//import GetMail from 'Components/Molecules/GMModal';
//import EPModal from 'Components/Molecules/EPModal';


/*
  useEffect(()=>{
    loadReCaptcha("6Le9sqwUAAAAAJL4XLtyMd70oSaEcWRIDMziBZux");
  Products
  })
*/

const App = (props)=> {
  return (
    
  <React.Fragment>

      <Switch>

      <Route 
          exact 
          path="/about" 
          component = {scrollToTopOnMount(About)}/>

       <Route 
          exact 
          path="/services" 
          component = {scrollToTopOnMount(Services)}/>

      <Route 
        exact 
        path="/stories" 
        component = {scrollToTopOnMount(Stories)}/>

        <Route 
        exact 
        path="/register" 
        component = {scrollToTopOnMount(UserForm)}/>




      <Route 
        exact 
        path="/post/:id" 
        key={Date.now()} 
        render = {(props)=>{

          const CP = scrollToTopOnMount(Post);

          return <CP {...props}/>

        }}/>



      <Route 
        exact 
        path="/story/:id" 
        key={Date.now()} 
        render = {
            (props) => {

                const CP = scrollToTopOnMount(Story);
                return <CP {...props }/>

            }
        }
      />



      <Route 
          exact 
          path="/blog" 
          component = {scrollToTopOnMount(Blog)}/>

          

        <Route 
          exact 
          path="/" 
          component = {scrollToTopOnMount(Landing)}/>
        
      
        
        <Route 
          component = {Error404}/>

      </Switch>

  </React.Fragment>
    );
  }

export default App;